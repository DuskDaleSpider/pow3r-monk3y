/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

function fixUrl(element, e, whoop){
	var origLink = element.getAttribute("data-url");
	element.setAttribute("data-url", origLink+"&PageID="+whoop+"&UserID="+userId);
	//setTimeout(function(){element.parentNode.remove();}, 1000);
}


//removes elements
Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};

//removes a collection of elements
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 0, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};
