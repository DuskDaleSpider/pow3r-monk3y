<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
 
require('UploadHandler.php');
 
$userID = $_GET["UserID"];
$pageID = $_GET["PageID"];
 
$userId = isset($_POST["UserID"]) ? $_POST["UserID"] : $_GET["UserID"];
$pageId = isset($_POST['PageID']) ? $_POST['PageID'] : $_GET["PageID"];

class CustomUploadHandler extends UploadHandler {
    protected function get_user_id() {
       if(isset($_POST["PageID"])){
       		return $_POST["UserID"].'/'.$_POST["PageID"];
       }else{
       		return $_GET["UserID"].'/'.$_GET["PageID"];
       }
    }
	
    protected function trim_file_name($file_path, $name, $size, $type, $error, $inded, $content_range) {
		$name = parent::trim_file_name($file_path, $name, $size, $type, $error, $inded, $content_range);
		$ext = pathinfo($name, PATHINFO_EXTENSION);
        $name = $this->randString(10).'.'.$ext;
        return $name;
    }
	
	protected function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
	{
	    $str = '';
	    $count = strlen($charset);
	    while ($length--) {
	        $str .= $charset[mt_rand(0, $count-1)];
	    }
	    return $str;
	}
}
 
error_reporting(E_ALL | E_STRICT);

$uploadHandler = new CustomUploadHandler(array(
	'user_dirs' => true,
	'max_number_of_files' => 500,
	'accept_file_types' => '/\.(gif|jpe?g|png)$/i'
));
