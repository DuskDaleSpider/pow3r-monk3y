<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$name = $_POST['Name'];
$gap = $_POST['Gap'];
$delay = $_POST['Delay'];
$trainType = $_POST['TrainType'];
$willDelete = $_POST['WillDelete'];
$deleteDelay = $_POST['DeleteDelay'];
$response = null;

if($user != null){

	$connection = ConnectToDB::connect();
	if($willDelete == "true") $willDelete = 1;
	else $willDelete = 0;
	$sql = 'INSERT INTO TrainGroups VALUES (NULL, "'.$name.'", '.$delay.', '.$gap.', "'.$trainType.'", "'.$willDelete.'", "'.$deleteDelay.'")';
	$connection->query($sql);
	if($connection->error){
		$response = array("success" => false, "error" => $connection->error);
		die(json_encode($response));
	}
	$sql = 'INSERT INTO TrainGroupAdmins VALUES ('.$connection->insert_id.', "'.$user->getUserID().'")';
	$connection->query($sql);
	if($connection->error){
		$response = array("success" => false, "error" => $connection->error);
		die(json_encode($response));
	}
	$connection->close();
	$response = array("success" => true);
}else{
	$response = array(
		"success" => false,
		"error" => "User is not logged in!"
	);
}

echo json_encode($response);

?>