<?php

require_once ( 'Facebook/Entities/AccessToken.php' );
require_once ( 'Facebook/FacebookSession.php' );
require_once ( 'Facebook/FacebookSDKException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookOtherException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookAuthorizationException.php');
require_once ( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once ( 'Facebook/HttpClients/FacebookCurl.php' );
require_once ( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( 'Facebook/FacebookRequest.php' );
require_once ( 'Facebook/FacebookResponse.php' );
require_once ( 'Facebook/GraphObject.php' );
require_once ( 'Facebook/GraphSessionInfo.php' );
require_once ( 'Facebook/FacebookServerException.php');
require_once ( 'Facebook/FacebookThrottleException.php');
require_once ( 'Facebook/FacebookRedirectLoginHelper.php');

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php' );

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$user = $_SESSION['User'];
$page = $_POST['Page'];
$delay = $_POST['Delay'];
$gap = $_POST['Gap'];
$willDelete = $_POST['WillDelete'];
$deleteDelay = $_POST['DeleteDelay'];
$shouldEnhance = $_POST['ShouldEnhance'];
$links = array();

$response = null;

if($user != null && $user != ""){
	$connection = ConnectToDB::connect();

	$now = time();
	$sql = '';
	$count = 0;
	$gap *= 60;
	$delay *= 60;
	$now += $delay;

	$sql = 'DELETE FROM ScheduledLinks WHERE PageID="'.$page.'" AND UserID="'.$user->getUserID().'";';

	$connection->query($sql);

	$sql = '';

	try{
		$filePath = '../fileUpload/server/php/files/'.$user->getUserID().'/links/'.$page.'.dat';
		$linksFile = fopen($filePath, 'r');
		$links = explode(',', fread($linksFile, filesize($filePath)));
		fclose($linksFile);
	}catch(Exception $e){
		 $response = array(
		 	"success" => false,
		 	"error" => "Could not open file!"
		 );
		 die(json_encode($response));
	}

	$pageAccessToken = '';

	$userPages = (new FacebookRequest($user->getSession(), "GET", "/me/accounts?limit=500"))->execute()->getGraphObject()->asArray();

	foreach($userPages["data"] as $temp){
		if($temp->id == $page){
			$pageAccessToken = $temp->access_token;
			break;
		}
	}

	foreach($links as $link){
		if($willDelete == "true") $willDelete = 1;
		if($shouldEnhance == "true") $link = 'https://www.hillandclark.com/LinksPage/?PageLink='.urlencode($link);
		$timeToPost = $now + ($gap * $count);
		$sql .= 'INSERT INTO ScheduledLinks VALUES'.
					'(NULL, "'.$user->getUserID().'", "'.$page.'", "'.$link.'", "'.$timeToPost.'", NULL, "'.$willDelete.'", "'.$deleteDelay.'", NULL);';
		$count++;
	}

	$connection->multi_query($sql);

	$connection->close();

	$response = array(
		"success" => true
	);
}else{
	$response = array(
		"success" => false,
		"error" => "You are not logged in."
	);
}

echo json_encode($response);
?>