<?php

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$delay = $_POST['Delay'];
$gap = $_POST['Gap'];
$willDelete = $_POST['WillDelete'];
$deleteDelay = $_POST['DeleteDelay'];
$status = array();

$response = null;

if($user != null && $user != ""){
	$connection = ConnectToDB::connect();

	$now = time();
	$sql = '';
	$count = 0;
	$gap *= 60;
	$delay *= 60;
	$now += $delay;

	//Get pages in folder
	$sql = 'SELECT * FROM PageFolderContents WHERE FolderID='.$folder;
	$pages = $connection->query($sql);

	//Delete current scheduled photos for each page
	$sql = "DELETE FROM ScheduledStatus WHERE UserID='".$user->getUserID()."' AND (";
	$count = 1;
	while($page = $pages->fetch_assoc()){
		$pageID = $page['PageID'];
		$sql .= "PageID='".$pageID."'";
		if($count != $pages->num_rows) $sql .= ' OR ';
		$count++;
	}
	$sql .= ')';
	$connection->query($sql);
	$pages->data_seek(0);

	$connection->query($sql);

	$sql = 'SELECT * FROM FolderStatus WHERE FolderID='.$folder;
	$status = $connection->query($sql);

	$count = 0;
	$sql = '';

	while($page = $pages->fetch_assoc()){
		$pageID = $page['PageID'];
		$pageAccessToken = $page['PageAccessToken'];
		$sql .= 'INSERT INTO ScheduledStatus VALUES ';
		while($message = $status->fetch_assoc()){
			if($willDelete == "true") $willDelete = 1;
			$timeToPost = $now + ($gap * $count);
			$statusText = $connection->real_escape_string($message['Status']);
			$sql .= '(NULL, "'.$user->getUserID().'", "'.$pageID.'", "'.$timeToPost.'", "'.$pageAccessToken.'", "'.$statusText.'", '.$willDelete.', "'.$deleteDelay.'", '.$folder.')';
			if($count != $status->num_rows - 1) $sql .= ', ';
			$count++;
		}
		$sql .= ';';
		$status->data_seek(0);
		$count = 0;
	}

	$connection->multi_query($sql);

	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		die(json_encode($response));
	}

	$connection->close();

	$response = array(
		"success" => true
	);
}else{
	$response = array(
		"success" => false,
		"error" => "You are not logged in."
	);
}

echo json_encode($response);
?>