<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$page = $_POST['Page'];
$response = null;

if($user != null && $user != ""){

	if($folder != null && $folder != "" && $page != null && $page != ""){
		$connection = ConnectToDB::connect();
		$sql = "DELETE FROM PageFolderContents WHERE PageID='".$page."' AND FolderID='".$folder."'";
		$connection->query($sql);
		if($connection->error){
			$response = array(
				"error" => $connection->error
			);
		}else{
			$response = array(
				"success" => true
			);
		}
		$connection->close();
	}else{
		$response = array(
			"error" => "invalid paremeters"
		);
	}

}else{
	$response = array(
		"error" => "user is not logged in"
	);
}

echo json_encode($response);

?>