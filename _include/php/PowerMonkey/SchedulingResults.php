<?php

namespace PowerMonkey;

require_once ( 'Result.php' );

use PowerMonkey\Result;



class SchedulingResults extends Result{

	private $delay;
	private $gap;
	private $willDelete;
	private $deleteDelay;

	public function __construct($params){
		$this->message = $params["message"];
		$this->err = $params["err"];
		$this->errMessage = $params["errMessage"];
		$this->delay = $params["Delay"];
		$this->gap = $params["Gap"];
		$this->willDelete = $params["WillDelete"];
		$this->deleteDelay = $params["DeleteDelay"];
	}

	public function getDelay(){ return $this->delay; }
	public function getGap(){ return $this->gap; }
	public function getWillDelete(){ return $this->willDelete; }
	public function getDeleteDelay(){ return $this->deleteDelay; }

	public function setDelay($param){ $this->delay = $param; }
	public function setGap($param){ $this->gap = $param; }
	public function setWillDelete($param){ $this->willDelete = $param; }
	public function setDeleteDelay($param){ $this->deleteDelay = $param; }

	public function getRawJSON(){
		return '{'.
					'"error": ' . $this->err . ', '.
					'"errorMessage": "' . $this->errMessage . '",'.
					'"message": "' . $this->message . '",'.
					'"Delay": ' . $this->delay . ', '.
					'"Gap": ' .$this->gap . ','.
					'"WillDelete": "' .$this->willDelete . '",'.
					'"DeleteDelay": ' .$this->deleteDelay . 
			   "}";
	}

}

?>