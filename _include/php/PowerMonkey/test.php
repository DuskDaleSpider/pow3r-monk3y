<?php

require_once ( '../Facebook/Entities/AccessToken.php' );
require_once ( '../Facebook/FacebookSession.php' );
require_once ( '../Facebook/FacebookSDKException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookOtherException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookAuthorizationException.php');
require_once ( '../Facebook/HttpClients/FacebookHttpable.php' );
require_once ( '../Facebook/HttpClients/FacebookCurl.php' );
require_once ( '../Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( '../Facebook/FacebookRequest.php' );
require_once ( '../Facebook/FacebookResponse.php' );
require_once ( '../Facebook/GraphObject.php' );
require_once ( '../Facebook/GraphSessionInfo.php' );
require_once ( '../Facebook/FacebookServerException.php');
require_once ( '../Facebook/FacebookThrottleException.php');
require_once ( '../Facebook/FacebookRedirectLoginHelper.php');
require_once ( '../Facebook/FacebookPermissionException.php');

require_once ( 'ConnectToDB.php' );


use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookPermissionException;

use PowerMonkey\ConnectToDB;

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$connection = ConnectToDB::connect();

$sql = 'SELECT n.UserID, u.AccessToken FROM UserNames AS n '.
	   'JOIN Users AS u ON n.UserID=u.ID';
$result = $connection->query($sql);

while($user = $result->fetch_assoc()){
	$session = new FacebookSession($user['AccessToken']);
	$sql = 'INSERT INTO PageAccessTokens VALUES ';
	$values = array();
	try{
		$response = (new FacebookRequest($session, 'GET', '/me/accounts?limit=500'))->execute()->getGraphObject()->asArray();
		$pages  = $response['data'];
		for($i = 0; $i < count($pages); $i++){
			$values[] =  '("'.$pages[$i]->id.'", "'.$user['UserID'].'", "'.$pages[$i]->access_token.'")';
		}
		$sql .= implode(', ', $values);
		$connection->query($sql);
	}catch(Exception $e){

	}
}
$connection->close();

?>