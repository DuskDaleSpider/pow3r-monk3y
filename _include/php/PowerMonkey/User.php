<?php

use Facebook\Entities\AccessToken;
use Facebook\Entities\SignedRequest;
use Facebook\FacebookSession;

namespace PowerMonkey;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PowerMonkey\User ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*\
 *	I) Prerequisites															*\
 *		1) Need to import FacebookSession and other Facebook PHP SDK files      *\
 *																				*\
 *																				*\
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

class User{

	/*
	 * Class Instance Variables
	 * -----------------------
	 * String userId = unique app-scope user id from facebook	 	
	 * String accessToken = unique oauth code for this user for this app from facebook
	 * Boolean isSubscribed = true if this user is paying us some dough
	 * FacebookSession session = A Facebook\FacebookSession object that is used for Facebook's GraphAPI
	 *
	 */

	private $userId;
	private $accessToken;
	private $isSubscribed;
	private $session;

	public function __construct($userId, $accessToken, $isSubscribed, $session){
		$this->userId = $userId;
		$this->accessToken = $accessToken;
		$this->isSubscribed = $isSubscribed;
		$this->session = $session;
	}

	public function getUserID(){ return $this->userId; }
	public function getAccessToken(){ return $this->accessToken; }
	public function getIsSubscribed(){ return $this->isSubscribed; }
	public function getSession(){ return $this->session; }

	public function setUserID($userId){ $this->userId = $userId; }
	public function setAccessToken($token){ $this->accessToken = $token; }
	public function setIsSubscribed($isSubscribed){ $this->isSubscribed = $isSubscribed; }
	public function setSession($session){ $this->session = $session; }
}

?>