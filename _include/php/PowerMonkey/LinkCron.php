<?php

require_once ( '../Facebook/Entities/AccessToken.php' );
require_once ( '../Facebook/FacebookSession.php' );
require_once ( '../Facebook/FacebookSDKException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookOtherException.php');
require_once ( '../Facebook/FacebookPermissionException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookAuthorizationException.php');
require_once ( '../Facebook/HttpClients/FacebookHttpable.php' );
require_once ( '../Facebook/HttpClients/FacebookCurl.php' );
require_once ( '../Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( '../Facebook/FacebookRequest.php' );
require_once ( '../Facebook/FacebookResponse.php' );
require_once ( '../Facebook/GraphObject.php' );
require_once ( '../Facebook/GraphSessionInfo.php' );
require_once ( '../Facebook/FacebookServerException.php');
require_once ( '../Facebook/FacebookThrottleException.php');
require_once ( '../Facebook/FacebookRedirectLoginHelper.php');

require_once ( 'ConnectToDB.php' );


use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookPermissionException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\ConnectToDB;

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

function postLinks($result){
	$userSessions = array();
	$connection = ConnectToDB::connect();

	while($row = $result->fetch_assoc()){
		if(!in_array($row["UserID"], $userSessions)){
			$sql = 'SELECT * FROM Users WHERE ID='.$row["UserID"];
			$user = $connection->query($sql);
			$user = $user->fetch_assoc();
			$userSessions[$user["ID"]] = new FacebookSession($user["AccessToken"]);
		}
		try{
			$post = (new FacebookRequest($userSessions[$row["UserID"]], "POST", "/".$row["PageID"]."/feed",
						array(
								"message" => $row["Message"],
								"access_token" => $row["PageAccessToken"],
								"link" => $row["Link"],
								"type" => "link"
							)))->execute()->getGraphObject()->asArray();
			if($row["WillDelete"] > 0) {
				$now = time();
				$timeToDelete = $now + ($row["DeleteDelay"] * 60);
				$sql = 'INSERT INTO ScheduledDeletes VALUES ('.
						'NULL, "'.$row["UserID"].'", "'.$row["PageID"].'", "'.$row["PageAccessToken"].'", "'.$post["id"].'", '.$timeToDelete.
					', NULL)';
				$connection->query($sql);
				echo $connection->error;
			}
		}catch(Exception $e){
			echo 'UserID: ' . $row['UserID'] . PHP_EOL . 'PageID: ' . $row['PageID'] . PHP_EOL;
			print_r($e);
		}	
	}

	$connection->close();
}

function postStatus($result){
	$userSessions = array();
	$connection = ConnectToDB::connect();

	while($row = $result->fetch_assoc()){
		if(!in_array($row["UserID"], $userSessions)){
			$sql = 'SELECT * FROM Users WHERE ID='.$row["UserID"];
			$user = $connection->query($sql);
			$user = $user->fetch_assoc();
			$userSessions[$user["ID"]] = new FacebookSession($user["AccessToken"]);
		}
		try{
			$post = (new FacebookRequest($userSessions[$row["UserID"]], "POST", "/".$row["PageID"]."/feed",
						array(
								"message" => $row["Message"],
								"access_token" => $row["PageAccessToken"],
								"type" => "status"
							)))->execute()->getGraphObject()->asArray();
			if($row["WillDelete"] > 0) {
				$now = time();
				$timeToDelete = $now + ($row["DeleteDelay"] * 60);
				$sql = 'INSERT INTO ScheduledDeletes VALUES ('.
						'NULL, "'.$row["UserID"].'", "'.$row["PageID"].'", "'.$row["PageAccessToken"].'", "'.$post["id"].'", '.$timeToDelete.
					', NULL)';
				$connection->query($sql);
				echo $connection->error;
			}
		}catch(Exception $e){
			echo 'UserID: ' . $row['UserID'] . PHP_EOL . 'PageID: ' . $row['PageID'] . PHP_EOL;
			print_r($e);
		}	
	}

	$connection->close();
}

$connection = ConnectToDB::connect();
$now = time();
$sql = 'SELECT l.*, t.AccessToken AS PageAccessToken '.
       'FROM ScheduledLinks AS l '.
       'JOIN PageAccessTokens AS t ON l.UserID=t.UserID '.
       'WHERE TimeToPost < ' . $now;
$result = $connection->query($sql);

echo $connection->error;
echo $result->num_rows;

if($result){
	//delete all links that we have retrieved
	$sql = 'DELETE FROM ScheduledLinks WHERE TimeToPost < ' . $now;
	$deleted = $connection->query($sql);

	//go through all links and post them to FB
	postLinks($result);
}

$sql = 'SELECT * FROM ScheduledTrainLinks WHERE TimeToPost < '. $now;
$result = $connection->query($sql);

if($result){
	$sql = 'DELETE FROM ScheduledTrainLinks WHERE TimeToPost < '.$now;
	$connection->query($sql);
	try{
		postLinks($result);
	}catch(Exception $e){
		var_dump($e);
	}
}

$sql = 'SELECT * FROM ScheduledStatus WHERE TimeToPost < '.$now;
$result = $connection->query($sql);

if($result){
	$sql = 'DELETE FROM ScheduledStatus WHERE TimeToPost < '.$now;
	$connection->query($sql);
	try{
		postStatus($result);
	}catch(Exception $e){
		
	}
}

$connection->close();