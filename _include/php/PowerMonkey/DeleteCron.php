<?php

require_once ( '../Facebook/Entities/AccessToken.php' );
require_once ( '../Facebook/FacebookSession.php' );
require_once ( '../Facebook/FacebookSDKException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookOtherException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookAuthorizationException.php');
require_once ( '../Facebook/HttpClients/FacebookHttpable.php' );
require_once ( '../Facebook/HttpClients/FacebookCurl.php' );
require_once ( '../Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( '../Facebook/FacebookRequest.php' );
require_once ( '../Facebook/FacebookResponse.php' );
require_once ( '../Facebook/GraphObject.php' );
require_once ( '../Facebook/GraphSessionInfo.php' );
require_once ( '../Facebook/FacebookServerException.php');
require_once ( '../Facebook/FacebookThrottleException.php');
require_once ( '../Facebook/FacebookRedirectLoginHelper.php');
require_once ( '../Facebook/FacebookPermissionException.php');

require_once ( 'ConnectToDB.php' );


use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookPermissionException;

use PowerMonkey\ConnectToDB;

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$userSessions = array();

$connection = ConnectToDB::connect();
$now = time();
$sql = 'SELECT * FROM ScheduledDeletes WHERE TimeToDelete < ' . $now;
$deleteQueue = $connection->query($sql);
echo $connection->error;
$sql = 'DELETE FROM ScheduledDeletes WHERE TimeToDelete < ' . $now;
$delted = $connection->query($sql);

if($deleteQueue){
	while($row = $deleteQueue->fetch_assoc()){
		if(!in_array($row["UserID"], $userSessions)){
			$sql = 'SELECT * FROM Users WHERE ID='.$row["UserID"];
			$user = $connection->query($sql)->fetch_assoc();
			$userSessions[$row["UserID"]] = new FacebookSession($user["AccessToken"]);
		}
		try{
			$delete = (new FacebookRequest($userSessions[$row["UserID"]], "DELETE", "/".$row["PostID"]))->execute()->getGraphObject()->asArray();
		}catch(Facebook\FacebookPermissionException $e){
			print_r($e);
		}
	}
}
?>