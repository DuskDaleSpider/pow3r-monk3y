<?php
	//delete a directory, even if files are still in them
	function delete_dir($path){
		//check if path is really a dir
		if(!is_dir($path)){
			throw new Exception("That was not a directory");
		}
		//check if path ends in '/'
		if(substr($path, strlen($path) -1, 1) != '/'){
			$path .= '/';
		}

		$files = glob($path . '*', GLOB_MARK);
		foreach($files as $file){
			if(is_dir($file)){
				delete_dir($file);
			}else{
				unlink($file);
			}
		}

		//remove dir
		rmdir($path);
	}

?>