<?

require_once ( '../Facebook/Entities/AccessToken.php' );
require_once ( '../Facebook/FacebookSession.php' );
require_once ( '../Facebook/FacebookSDKException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookOtherException.php');
require_once ( '../Facebook/FacebookRequestException.php');
require_once ( '../Facebook/FacebookAuthorizationException.php');
require_once ( '../Facebook/HttpClients/FacebookHttpable.php' );
require_once ( '../Facebook/HttpClients/FacebookCurl.php' );
require_once ( '../Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( '../Facebook/FacebookRequest.php' );
require_once ( '../Facebook/FacebookResponse.php' );
require_once ( '../Facebook/GraphObject.php' );
require_once ( '../Facebook/GraphSessionInfo.php' );
require_once ( '../Facebook/FacebookServerException.php');
require_once ( '../Facebook/FacebookThrottleException.php');
require_once ( '../Facebook/FacebookRedirectLoginHelper.php');

require_once ( 'ConnectToDB.php' );


use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\ConnectToDB;

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$userSessions = array();
//$needsDeleting = array();

$connection = ConnectToDB::connect();
$now = time();
$sql = 'SELECT * FROM ScheduledPhotos WHERE TimeToPost < ' .$now;
$result = $connection->query($sql);
$sql = 'DELETE FROM ScheduledPhotos WHERE TimeToPost < ' . $now;
$deleted = $connection->query($sql); 
if($result){
	while($row = $result->fetch_assoc()){
		if(!in_array($row["UserID"], $userSessions)){
			$sql = 'SELECT * FROM Users WHERE ID='.$row["UserID"];
			$user = $connection->query($sql);
			$user = $user->fetch_assoc();
			$userSessions[$user["ID"]] = new FacebookSession($user["AccessToken"]);
		}

		//make sure link is valid
		$explodedPicLink = explode('/', $row['PicLink']);
		$fileName = $explodedPicLink[count($explodedPicLink)-1];
		$path = '';
		if($row['FolderID'] == null){
			$path = '../../fileUpload/server/php/files/'.$row['UserID'].'/'.$row['PageID'].'/'.$fileName;
		}else{
			$path = '../../fileUpload/server/php/files/'.$row['UserID'].'/folders/'.$row['FolderID'].'/'.$fileName;
		}

		if(!is_file($path)) continue;

		try{
			$post = (new FacebookRequest($userSessions[$row["UserID"]], "POST", "/".$row["PageID"]."/photos",
							array(
									"message" => $row["Message"],
									"access_token" => $row["PageAccessToken"],
									"url" => $row["PicLink"]
								)))->execute()->getGraphObject()->asArray();
		}catch(Exception $e){
			echo 'UserID: ' . $row['UserID'] . PHP_EOL . 'PageID: ' . $row['PageID'] . PHP_EOL;
			print_r($e);
		}
		if($row["WillDelete"] > 0) {
			$timeToDelete = $now + ($row["DeleteDelay"] * 60);
			if($row['FolderID'] == null) $row['FolderID'] = 'NULL';
			$sql = 'INSERT INTO ScheduledDeletes VALUES ('.
					'NULL, "'.$row["UserID"].'", "'.$row["PageID"].'", "'.$row["PageAccessToken"].'", "'.$post["id"].'", '.$timeToDelete.
				', '.$row['FolderID'].')';
			$connection->query($sql);
			echo $connection->error;
		}

		/*delete this photo from this server
		$explodedPicURL = explode('/', $row["PicLink"]);
		if($row['FolderID'] == NULL){
			$url = '../../fileUpload/server/php/files/'.$row["UserID"].'/'.$row["PageID"].'/'.$explodedPicURL[count($explodedPicURL)-1];
			unlink($url);
		}else{
			$url = '../../fileUpload/server/php/files/'.$row['UserID'].'/folders/'.$row['FolderID'].'/'.$explodedPicURL[count($explodedPicURL)-1];
			if(!in_array($url, $needsDeleting)){
				array_push($needsDeleting, $url);
			}
		}*/
	}
}

//delete folder photos
/*
foreach($needsDeleting as $pic){
	unlink($pic);
}*/

$connection->close();
?>