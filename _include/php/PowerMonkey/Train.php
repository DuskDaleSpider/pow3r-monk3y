<?php

namespace PowerMonkey;

class Train{

	/*
	 *Instance Variables
	 * Posts - an array of Post objects that are to posted on this train
	 * User - a User object that is used for access tokens and etc.
	 * PageId- The Facebook id for a page
	 */
	private $posts;
	private $user;
	private $pageId;

	public function __construct($posts, $user, $pageId){
		$this->posts = $posts;
		$this->user = $user;
		$this->pageId = $pageId;
	}

	public function getPosts(){ return $this->posts; }
	public function getUser(){ return $this->user; }
	public function getPageID(){ return $this->pageId; }

	public function setPosts($posts){ $this->posts = $posts; }
	public function setUser($user){ $this->user = $user; }
	public function setPosts($pageId){ $this->pageId = $pageId; }

}

?>