<?php

namespace PowerMonkey;

class Result{
	private $err;
	private $errMessage;
	private $message;

	public function __construct($params){
		$this->err = isset($params["err"]) ? $params["err"] : -1;
		$this->errMessage = isset($params["errMessage"]) ? $params["errMessage"] : "";
		$this->message = isset($params["message"]) ? $params["message"] : "";
	}

	public function getMessage(){ return $this->message; }
	public function geterrMessage(){ return $this->errMessage; }
	public function getError(){ return $this->err; }

	public function setMessage($message){ $this->message = $message; }
	public function setErrorMessage($errMessage){ $this->errMessage = $errMessage; }
	public function setError($err){ $this->err = $err; }

	public function getRawJSON(){
		return '{'.
					'"error": ' . $this->err . ', '.
					'"errorMessage": "' . $this->errMessage . '",'.
					'"message": "' . $this->message . '"'.
			   "}";
	}

}
