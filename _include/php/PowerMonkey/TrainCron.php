<?php

require_once ('ConnectToDB.php');

use PowerMonkey\ConnectToDB;

$connection = ConnectToDB::connect();
$now = time();



$sql = 'SELECT * FROM Trains WHERE CloseTime < '.$now;
$Trains = $connection->query($sql);
$sql = 'DELETE FROM Trains WHERE CloseTime < '.$now;
$connection->query($sql);
$finalSql = '';
if($Trains->num_rows > 0){
	while($row = $Trains->fetch_assoc()){
		$gap = $row['Gap'] * 60;
		$delay = $row['Delay'] * 60;
		$now += $delay;
		$willDelete = $row['WillDelete'];
		$deleteDelay = $row['DeleteDelay'];
		$sql = 'SELECT * FROM TrainLinks WHERE TrainID='.$row['ID'];
		$result = $connection->query($sql);
		if($result->num_rows > 1){
			$sql = 'DELETE FROM TrainLinks WHERE TrainID='.$row['ID'];
			$connection->query($sql);
			$links = array();
			while($link = $result->fetch_assoc()){
				$links[] = $link;
			}

			for($x = 0; $x < count($links); $x++){
				$count = 0;
				$finalSql .= 'INSERT INTO ScheduledTrainLinks (UserID, PageID, Link, TimeToPost, PageAccessToken, Message, WillDelete, DeleteDelay) VALUES ';
				$values = array();
				for($y = 0; $y < count($links); $y++){
					if($x != $y){
						$timeToPost = $now + ($gap * $count);
						$pageLink = 'https://www.hillandclark.com/LinksPage/?PageID='.$links[$y]["PageID"];
						$values[] = '("'.$links[$x]["UserID"].'", "'.$links[$x]["PageID"].'", '.
								'"'.$pageLink.'", "'.$timeToPost.'", "'.$links[$x]["PageAccessToken"].'", '.
								'NULL, "'.$willDelete.'", "'.$deleteDelay.'")';
						$count++;
					}
				}
				$finalSql .= implode(', ', $values);
				$finalSql .= ';';
			}
			$connection->multi_query($finalSql);
		}
	}
}

$connection->close();