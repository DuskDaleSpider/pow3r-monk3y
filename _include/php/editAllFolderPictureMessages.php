<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$message = $_POST['Message'];
$response = null;

if($user != null && $user != ""){
	if($folder != null && $folder != "" && $message != null && $message != ""){
		$connection = ConnectToDB::connect();
		$sql = "UPDATE ScheduledPhotos SET Message='".$message."' WHERE UserID='".$user->getUserID()."' AND FolderID=".$folder;
		$connection->query($sql);
		if($connection->error){
			$response = array("error" => $connection->error);
		}else{
			$response = array("success" => true);
		}
		$connection->close();
	}else{
		$response = array("error" => "invalid parameters");
	}
}else{
	$response = array("error" => "user is not logged in");
}

echo json_encode($response);
?>