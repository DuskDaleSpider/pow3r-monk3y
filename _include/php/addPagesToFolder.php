<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$pages = $_POST['Pages'];
$folder = $_POST['Folder'];
$response = null;

if($user != null && $user != ""){
	if($folder != null && $folder != "" && $pages != null && $pages != ""){
		$connection = ConnectToDB::connect();
		$sql = 'INSERT INTO PageFolderContents VALUES ';
		for($i = 0; $i < count($pages); $i++){
			$id = $pages[$i]['ID'];
			$name = $connection->real_escape_string($pages[$i]['Name']);
			$token = $pages[$i]['Token'];
			$sql .= "(".$folder.", '".$id."', '".$name."', '".$token."')";
			if($i != count($pages) - 1) $sql .= ', ';
		}
		$connection->query($sql);
		if($connection->error){
			$response = array(
				"error" => $connection->error
			);
		}else{
			$response = array(
				"success" => true
			);
		}
	}else{
		$response = array(
			"error" => "invalid parameters"
		);
	}
}else{
	$response = array(
		"error" => "user is not logged in"
	);
}

echo json_encode($response);
?>