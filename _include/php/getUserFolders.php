<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$response = null;

if($user != null && $user != ""){
	//get all of the user's folders
	$connection = ConnectToDB::connect();
	$sql = "SELECT * FROM PageFolders WHERE UserID='".$user->getUserID()."'";
	$folders = $connection->query($sql);
	if($connection->error){
		$response = array("error" => "Something went wrong. Plese try again.");
	}else{
		$response = array("data" => array());
		while($folder = $folders->fetch_assoc()){
			$folderData = array(
				"id" => $folder['ID'],
				"name" => $folder['Name'],
				"pages" => array()
			);
			//get all pages for folder
			$sql = 'SELECT * FROM PageFolderContents WHERE FolderID='.$folder['ID'];
			$pages = $connection->query($sql);
			if($connection->error){
				$response['sql_error'] = true;
				if($response['errors'] == null) $response['errors'] = array();
				array_push($response['errors'], $connection->error);
			}else{
				while($page = $pages->fetch_assoc()){
					$pageData = array(
						"id" => $page['PageID'],
						"name" => $page['PageName']
					);
					array_push($folderData['pages'], $pageData);
				}
			}
			array_push($response['data'], $folderData);
		}
	}
	$connection->close();
}else{
	$response = array("error" => "user is not logged in");
}

echo json_encode($response);
?>