<?

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'INSERT INTO TrainGroupMembers VALUES ('.$train.', "'.$user->getUserID().'")';
	$connection->query($sql);
	if($connection->error){
		$response = array("success" => false, "error" => $connection->error);
	}else{
		$response = array("success" => true);
	}
	$connection->close();
}else{
	$response = array("success" => false, "error" => "User is not logged in");
}

echo json_encode($response);

?>