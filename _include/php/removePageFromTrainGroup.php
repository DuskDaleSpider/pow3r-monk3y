<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$page = $_POST['Page'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'DELETE FROM PageTrainLinks WHERE TrainID='.$train.' AND PageID="'.$page.'"';
	$connection->query($sql);
	$connection->close();
	$response = array("success" => true);
}

echo json_encode($response);
?>