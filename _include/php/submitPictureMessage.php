<?php

require_once('PowerMonkey/ConnectToDB.php');
require_once('PowerMonkey/User.php');
require_once('PowerMonkey/Result.php');

use PowerMonkey\ConnectToDB;
use PowerMonkey\User;
use PowerMonkey\Result;

session_start();

$user = $_SESSION["User"];
$ID = $_POST["ID"];
$Message = $_POST["Message"];
$response;

if($user != null && $user != ""){
	if($ID != null && $ID != "" && $Message != null && $Message != ""){
		$connection = ConnectToDB::connect();
		$sql = 'UPDATE ScheduledPhotos SET Message="'.$Message.'" WHERE ID=' . $ID;
		$result = $connection->query($sql);
		if($connection->error){
			$response = new Result([
					"err" => 1,
					"errMessage" => $connection->error
				]);
		}else{
			$response = new Result(["message" => "Success"]);
		}
	}else{
		$response = new Result([
				"err" => 1,
				"errMessage" => "Invalid Parameters"
			]);
	}
}else{
	$response = new Result([
			"err" => 1,
			"errMessage" => "User is not valid"
		]);
}

echo $response->getRawJSON();

$connection->close();
?>