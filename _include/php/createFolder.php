<?php

require_once('PowerMonkey/ConnectToDB.php');
require_once('PowerMonkey/User.php');

use PowerMonkey\ConnectToDB;
use PowerMonkey\User;

session_start();

$user = $_SESSION['User'];
$name = $_POST['Name'];
$folderType = $_POST['FolderType'];
$response = null;

if($user != null && $user != ""){
	if($name != "" && $name != null){
		$connection = ConnectToDB::connect();
		$sql = "INSERT INTO PageFolders VALUES (NULL, '".$user->getUserID()."', '".$name."', ".$folderType.")";
		$connection->query($sql);
		if($connection->error){
			$response = array(
				"error" => "Something went wrong. Please try again."
			);
		}else{
			$response = array(
				"success" => true
			);
		}
		$connection->close();
	}else{
		$response = array(
			"error" => "invalid parameters"
		);
	}
}else{
	$response = array(
		"error" => "user is not logged in"
	);
}

echo json_encode($response);
?>