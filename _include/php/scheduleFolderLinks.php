<?php

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$delay = $_POST['Delay'];
$gap = $_POST['Gap'];
$willDelete = $_POST['WillDelete'];
$deleteDelay = $_POST['DeleteDelay'];
$shouldEnhance = $_POST['ShouldEnhance'];
$links = array();

$response = null;

if($user != null && $user != ""){
	$connection = ConnectToDB::connect();

	$now = time();
	$sql = '';
	$count = 0;
	$gap *= 60;
	$delay *= 60;
	$now += $delay;

	//Get pages in folder
	$sql = 'SELECT * FROM PageFolderContents WHERE FolderID='.$folder;
	$pages = $connection->query($sql);

	//Delete current scheduled photos for each page
	$sql = "DELETE FROM ScheduledLinks WHERE UserID='".$user->getUserID()."' AND (";
	$count = 1;
	while($page = $pages->fetch_assoc()){
		$pageID = $page['PageID'];
		$sql .= "PageID='".$pageID."'";
		if($count != $pages->num_rows) $sql .= ' OR ';
		$count++;
	}
	$sql .= ')';
	$connection->query($sql);
	$pages->data_seek(0);


	$sql = '';

	try{
		$filePath = '../fileUpload/server/php/files/'.$user->getUserID().'/links/folders/'.$folder.'.dat';
		$linksFile = fopen($filePath, 'r');
		$links = explode(',', fread($linksFile, filesize($filePath)));
		fclose($linksFile);
	}catch(Exception $e){
		 $response = array(
		 	"success" => false,
		 	"error" => "Could not open file!"
		 );
		 die(json_encode($response));
	}

	$count = 0;

	while($page = $pages->fetch_assoc()){
		$sql .= 'INSERT INTO ScheduledLinks VALUES ';
		foreach($links as $link){
			if($willDelete == "true") $willDelete = 1;
			if($shouldEnhance == "true") $link = 'https://www.hillandclark.com/LinksPage/?PageLink='.urlencode($link);
			$pageID = $page['PageID'];
			$pageAccessToken = $page['PageAccessToken'];
			$timeToPost = $now + ($gap * $count);
			$sql .= '(NULL, "'.$user->getUserID().'", "'.$pageID.'", "'.$link.'", "'.$timeToPost.'", NULL, "'.$willDelete.'", "'.$deleteDelay.'", '.$folder.')';
			if($count != count($links) - 1){ $sql .= ', ';}
			$count++;
		}
		$sql .= ';';
		$count = 0;
	}

	$connection->multi_query($sql);

	$connection->close();

	$response = array(
		"success" => true
	);
}else{
	$response = array(
		"success" => false,
		"error" => "You are not logged in."
	);
}

echo json_encode($response);
?>