<?php

	require_once ( 'Facebook/Entities/AccessToken.php' );
require_once ( 'Facebook/FacebookSession.php' );
require_once ( 'Facebook/FacebookSDKException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookOtherException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookAuthorizationException.php');
require_once ( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once ( 'Facebook/HttpClients/FacebookCurl.php' );
require_once ( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( 'Facebook/FacebookRequest.php' );
require_once ( 'Facebook/FacebookResponse.php' );
require_once ( 'Facebook/GraphObject.php' );
require_once ( 'Facebook/GraphSessionInfo.php' );
require_once ( 'Facebook/FacebookServerException.php');
require_once ( 'Facebook/FacebookThrottleException.php');
require_once ( 'Facebook/FacebookRedirectLoginHelper.php');


require_once( 'PowerMonkey/User.php' );

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\User;

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

session_start();
error_reporting(-1);

$user = $_SESSION['User'];
$page = $_POST['Page'];
$response = null;

if($user != null && $user != ''){ //picture?type=large&redirect=true
	$picture = (new FacebookRequest($user->getSession(), 'GET', '/'.$page.'/picture?redirect=false&type=large'))->execute()->getGraphObject();
	$picture = $picture->asArray();
	$response = array(
		"id" => $page,
		"url" => $picture['url']
	);
}

echo json_encode($response);
?>