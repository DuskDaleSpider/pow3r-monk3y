<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$pages = $_POST['Pages'];
var_dump($pages);
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();

	$sql = 'SELECT * FROM PageAccessTokens WHERE UserID="'.$user->getUserID().'"';
	$results = $connection->query($sql);
	echo $results->num_rows;
	if($results->num_rows == 0){
		$sql = 'INSERT INTO PageAccessTokens VALUES ';
		$values = array();
		$userId = $user->getUserID();
		for($i = 0; $i < count($pages); $i++){
			$values[] = '("'.$pages[$i]['id'].'", "'.$userId.'", "'.$pages[$i]['token'].'")';
		}
		$sql .= implode(', ', $values);
		$connection->query($sql);
		if($connection->error){
			$response = array("success" => false, "error" => $connection->error);
		}else{
			$response = array("success" => true);
		}
	}else{
		$sql = '';
		$values = array();
		for($i = 0; $i < count($pages); $i++){
			$values[] = 'UPDATE PageAccessTokens SET AccessToken="'.$pages[$i]['token'].'" WHERE UserID="'.$user->getUserID().'" AND PageID="'.$pages[$i]['id'].'"';
		}
		$sql = implode(';', $values).';';
		$connection->multi_query($sql);
		$response = array("success" => true);
	}
	$connection->close();
}else{
	$response = array("success" => false, "error" => "User is not logged in!");
}

echo json_encode($response);
?>