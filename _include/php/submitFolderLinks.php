<?php

require_once( 'PowerMonkey/User.php' );

use PowerMonkey\User;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$links = $_POST['Links'];
$currentLinks = null;
$response = array();

if($user != null && $user != ""){
	for($i = 0; $i < count($links); $i++){
		if($links[$i] == "") unset($links[$i]);
	} 
	$links = array_values($links);

	for($i = 0; $i < count($links); $i++){
		if(strpos($links[$i], "http") === false){
			$links[$i] = 'http://' . $links[$i];
		}
	}

	$links = implode(',', $links);
	try{
		$filePath = '../fileUpload/server/php/files/'.$user->getUserID().'/'.
						'links/folders/'.$folder.'.dat';
		$linksFile = fopen($filePath, 'r+');
		if(filesize($filePath) > 0){
			$currentLinks = fread($linksFile, filesize($filePath));
		}
		if($currentLinks != null) $links = ',' . $links;
		fwrite($linksFile, $links);
		fclose($linksFile);

		$response = array(
			"success" => true,
			"message" => "Links have been successfully added!"
		);
	}catch(Exception $e){
		$response = array(
			"success" => false,
			"error" => $e->getMessage()
		);
	}
}

echo json_encode($response);

?>