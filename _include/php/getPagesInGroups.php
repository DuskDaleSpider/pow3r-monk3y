<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$pages = array();
$response = null;

if($user != null && $user != ""){
	$connection = ConnectToDB::connect();
	$sql = "SELECT DISTINCT pfc.PageID ".
			"FROM PageFolders ".
				"INNER JOIN PageFolderContents as pfc on PageFolders.ID=pfc.FolderID ".
			"WHERE UserID='".$user->getUserID()."' ".
			"LIMIT 500";
	$groupedPages = $connection->query($sql);

	while($page = $groupedPages->fetch_assoc()){
		array_push($pages, $page['PageID']);
	}

	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		die(json_encode($response));
	}

	$connection->close();

	$response = array(
		"success" => true,
		"pages" => $pages
	);
}else{
	$response = array(
		"success" => false,
		"error" => "User Not Logged In!"
	);
}

echo json_encode($response);
?>