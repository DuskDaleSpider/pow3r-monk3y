<?php

require_once ( 'Facebook/Entities/AccessToken.php' );
require_once ( 'Facebook/FacebookSession.php' );
require_once ( 'Facebook/FacebookSDKException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookOtherException.php');
require_once ( 'Facebook/FacebookAuthorizationException.php');
require_once ( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once ( 'Facebook/HttpClients/FacebookCurl.php' );
require_once ( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( 'Facebook/FacebookRequest.php' );
require_once ( 'Facebook/FacebookResponse.php' );
require_once ( 'Facebook/GraphObject.php' );
require_once ( 'Facebook/GraphSessionInfo.php' );
require_once ( 'Facebook/FacebookServerException.php');
require_once ( 'Facebook/FacebookThrottleException.php');
require_once ( 'Facebook/FacebookRedirectLoginHelper.php');

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');
require_once('PowerMonkey/Result.php');

require_once('stripe-php/init.php');

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;
use PowerMonkey\Result;

\Stripe\Stripe::setApiKey("sk_live_rvHkA2K2f0Dtx5crsXlSPNYg");

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

session_start();

$user = $_GET["User"];
$token = $_GET['AccessToken'];

try{
	$session = new FacebookSession($token);
	$session = $session->getLongLivedSession();
	$token = $session->getToken();

}catch(Exception $e){

}

if($user == null || $user == ""){
	$response = new Result(["err"=>1, "errMessage" => "User session variable not set!"]);
	echo $response->getRawJSON();
	die();
}

$connection = ConnectToDB::connect();

$sql = 'SELECT * FROM Users WHERE ID="' . $user . '"';

$result = $connection->query($sql);

$response = null;

try{
	$name = (new FacebookRequest(new FacebookSession($token), 'GET', '/me'))->execute()->getGraphObject()->asArray();
	$sql = 'INSERT INTO UserNames VALUES ("'.$user.'", "'.$connection->real_escape_string($name['name']).'", 0)';
	$connection->query($sql);	
}catch(Exception $e){

}

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	$stripeID = $row["StripeID"];
	$sql = 'UPDATE Users SET AccessToken="'. $token .'" WHERE ID="'.$user.'";';
	$result = $connection->query($sql);
	if(!$result){
		$response = array(
			"err" => 1,
			"errMessage" => $connection->error
		);
	}else{

		if($stripeID != null && $stripeID != ""){
			$customer = \Stripe\Customer::retrieve($stripeID);
			$subscriptions = $customer->subscriptions->all();
			foreach($subscriptions->data as $subscription){
				if($subscription->plan->name == "Pow3r Monk3y Pack"){
					if($subscription->status == "active"){
						$isSubscribed = true;
						break;
					}else
						$isSubscribed = false;
				}else{
					$isSubscribed = false;
				}
			}	
		}
		$response = array(
			"isSubscribed"=> $isSubscribed,
			"token" => $token
		);
		echo json_encode($response);
		die();
	}
}else{
	$sql = 'INSERT INTO Users VALUES ("'.$user.'", "'.$token.'", NULL);';
	$result = $connection->query($sql);
	if(!$result){
		$response = new Result([
			"err" => 1,
			"errMessage" => $connection->error
		]);
	}else{
		$response = new Result(["message" => "User has been added!"]);
	}
}

$connection->close();

echo $response->getRawJSON();

?>