<?php

require_once ('PowerMonkey/User.php');

use PowerMonkey\User;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$link = $_POST['Link'];

$response = null;

if($user != null && $user != ""){
	try{
		$filePath = '../fileUpload/server/php/files/'.$user->getUserID().
			'/links/folders/'.$folder.'.dat';
		$linksFile = fopen($filePath, 'r');
		$links = explode(',', fread($linksFile, filesize($filePath)));
		fclose($linksFile);
		for($i = 0; $i < count($links); $i++){
			if($links[$i] == $link){
				array_splice($links, $i, 1);
				break;
			}
		}
		$linksFile = fopen($filePath, 'w');
		fwrite($linksFile, implode(',', $links));
		fclose($linksFile);

		$response = array(
			"success" => true
		);

	}catch(Exception $e){
		$response = array(
			"success" => false,
			"error" => $e->getMessage()
		);
	}
}else{
	$response = array(
		"success" => false,
		"error" => "User is not logged in!"
	);
}

echo json_encode($response);
?>