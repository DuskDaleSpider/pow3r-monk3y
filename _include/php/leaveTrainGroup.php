<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'DELETE FROM PageTrainLinks WHERE TrainID='.$train.' AND UserID="'.$user->getUserID().'";'.
		   'DELETE FROM TrainGroupMembers WHERE TrainID='.$train.' AND UserID="'.$user->getUserID().'";';
    $connection->multi_query($sql);
    $connection->close();
    $response = array("success" => true);
}else{
	$response = array("success" => false, "error" => "User is not logged in");
}

echo json_encode($response);

?>