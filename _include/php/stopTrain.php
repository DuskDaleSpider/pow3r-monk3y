<?php

require_once ('PowerMonkey/User.php');
require_once ('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$response = null;

if($user != null && $user != ""){

	if($train != null && $train != ""){
		$connection = ConnectToDB::connect();
		$sql = 'DELETE FROM ScheduledTrainLinks WHERE UserID="'.$user->getUserID().'" AND TrainID='.$train.'';
		$connection->query($sql);
		$connection->close();
		$response = array(
			"success" => true
		);
	}else{
		$response = array(
			"error" => "That's not a valid train!"
		);
	}
}else{
	$response = array(
		"error" => "You are not logged in. Reloading page..",
		"errorNum" => 100
	);
}

echo json_encode($response);
?>