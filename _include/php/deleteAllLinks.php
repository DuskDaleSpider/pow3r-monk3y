<?php

require_once('PowerMonkey/User.php');

use PowerMonkey\User;

session_start();

$user = $_SESSION['User'];
$page = $_GET['Page'];
$response = null;

if($user != null && $user != ""){

	if($page != null && $page != ""){

		$filepath = '../fileUpload/server/php/files/'.$user->getUserID().'/links/'.$page.'.dat';
		fclose(fopen($filepath, 'w'));
		$response = array(
			'success' => true
		);
	}else{
		$response = array(
			'error' => 'Invalid parameters'
		);
	}

}else{
	$response = array(
		'error' => 'User is not logged in'
	);
}

echo json_encode($response);

?>