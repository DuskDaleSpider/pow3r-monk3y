<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$pic = $_POST['Picture'];
$response = null;

if($user != null && $user != ""){
	if($folder != null && $folder != "" && $pic != null && $pic != ""){
		$connection = ConnectToDB::connect();
		$sql = "DELETE FROM ScheduledPhotos WHERE UserID='".$user->getUserID()."' AND FolderID='".$folder."' AND PicLink='".$pic."'";
		$connection->query($sql);
		if($connection->error){
			$response = array("error" => $connection->error);
		}else{
			$response = array("success" => true);
		}
	}else{
		$response = array("error" => "invalid parameters");
	}
}else{
	$response = array("error" => "user is not logged in");
}

echo json_encode($response);
?>