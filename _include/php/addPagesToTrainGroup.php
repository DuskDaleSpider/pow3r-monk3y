<?php

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$pages = $_POST['Pages'];
$response = null;

$trainLimit = 0;
$trainCurrent = 0;
if($user != null && $user != ""){
	if($train != null && $train != ""){
		if(count($pages) > 0){

			$connection = ConnectToDB::connect();

			$sql = 'INSERT INTO PageTrainLinks (TrainID, UserID, PageID, PageName, PageAccessToken) VALUES ';
			$values = array();
			for($x = 0; $x < count($pages); $x++){
				$pageID = $pages[$x]["ID"];
				$pageName = $connection->real_escape_string($pages[$x]["Name"]);
				$token = $pages[$x]['Token'];
				array_push($values, '('.$train.', "'.$user->getUserID().'", "'.$pageID.'", "'.$pageName.'", "'.$token.'")');
			}
			$sql .= implode(',', $values).';';
			$connection->query($sql);

			if($connection->error){
				$response = array(
					"error" => $connection->error
				);
			}else{
				$response = array(
					"success" => true
				);
			}
			$connection->close();
		}else{
			$response = array(
				"error" => "There are no pages to add!"
			);
		}
	}else{
		$response = array(
			"error" => "Invalid Parameters"
		);
	}
}else{
	$response = array(
		"error" => "User is not logged in!"
	);
}

echo json_encode($response);

?>