<?php
	require_once ( 'Facebook/Entities/AccessToken.php' );
require_once ( 'Facebook/FacebookSession.php' );
require_once ( 'Facebook/FacebookSDKException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookOtherException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookAuthorizationException.php');
require_once ( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once ( 'Facebook/HttpClients/FacebookCurl.php' );
require_once ( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( 'Facebook/FacebookRequest.php' );
require_once ( 'Facebook/FacebookResponse.php' );
require_once ( 'Facebook/GraphObject.php' );
require_once ( 'Facebook/GraphSessionInfo.php' );
require_once ( 'Facebook/FacebookServerException.php');
require_once ( 'Facebook/FacebookThrottleException.php');
require_once ( 'Facebook/FacebookRedirectLoginHelper.php');


require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/Result.php');
require_once( 'PowerMonkey/SchedulingResults.php');
require_once( 'PowerMonkey/ConnectToDB.php');

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\User;
use PowerMonkey\Result;
use PowerMonkey\SchedulingResults;
use PowerMonkey\ConnectToDB;

session_start();

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$user = $_SESSION["User"];
$id = $_POST["id"];
$response = array();

if($user != null && $user != "" && $id != null && $id != ""){
	$session = new FacebookSession($user->getAccessToken());
	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM ScheduledPhotos WHERE ID='.$id;
	$now = time();

	$result = $connection->query($sql);
	while($row = $result->fetch_assoc()){
		if($row["TimeToPost"] < $now){
			$post = (new FacebookRequest($session, "POST", "/".$row["PageID"]."/photos",
						array(
								"message" => $row["Message"],
								"access_token" => $row["PageAccessToken"],
								"url" => $row["PicLink"]
							)))->execute()->getGraphObject()->asArray();
			if($row["WillDelete"] > 0) {
				$timeToDelete = $now + ($row["DeleteDelay"] * 60);
				$sql = 'INSERT INTO ScheduledDeletes VALUES ('.
						'NULL, "'.$row["UserID"].'", "'.$row["PageID"].'", "'.$row["PageAccessToken"].'", "'.$post["id"].'", '.$timeToDelete.
					')';
				$connection->query($sql);
				
				$sql = 'SELECT * FROM ScheduledDeletes WHERE PostID="'.$post["id"].'"';
				$scheduledDelete = $connection->query($sql);
				while($deleteRow = $scheduledDelete->fetch_assoc()){
					$response = $deleteRow;
				}
			}
		}
	}

	echo json_encode($response);
}

?>