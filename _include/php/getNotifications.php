<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$lastChecked = $_GET['lastChecked'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'SELECT COUNT(*) as Count FROM Notifications WHERE UserID="'.$user->getUserID().'" AND Time > '.$lastChecked.' AND Seen=0';
	$results = $connection->query($sql)->fetch_assoc();
	$connection->close();
	$response = array("success" => true, "count" => $results['Count'], "lastChecked" => time());
}else{
	$response = array("success" => false, "error" => "User is not logged in");
}

echo json_encode($response);

?>