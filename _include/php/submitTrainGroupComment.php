<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$comment = $_POST['Comment'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$now = time();
	$sql = 'INSERT INTO TrainGroupComments VALUES (NULL, '.$train.', "'.$comment.'", '.$now.', "'.$user->getUserID().'")';
	$connection->query($sql);
	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		$connection->close();
		die(json_encode($response));
	}
	$connection->close();
	$response = array("success" => true);
	echo json_encode($response);
}