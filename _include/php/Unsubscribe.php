<?php

require_once('stripe-php/init.php');
require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

\Stripe\Stripe::setApiKey("sk_live_rvHkA2K2f0Dtx5crsXlSPNYg");

$user = $_SESSION["User"];
$response = array();

if($user != null && $user != ""){
	try{
		$connection = ConnectToDB::connect();
		$sql = "SELECT StripeID FROM Users WHERE ID=".$user->getUserID();
		$results = $connection->query($sql);
		while($row = $results->fetch_assoc()){
			$customer = \Stripe\Customer::retrieve($row["StripeID"]);
			$subId = $customer->subscriptions->data[0]->id;
			$customer->subscriptions->retrieve($subId)->cancel(array(
				"at_period_end" => true
			));
		}
		$response["success"] = true;
	}catch(Exception $e){
		$response["success"] = false;
	}
}

echo json_encode($response);

?>