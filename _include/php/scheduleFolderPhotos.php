<?php

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST["Folder"];
$delay = $_POST["Delay"];
$gap = $_POST["Gap"];
$willDelete = $_POST["WillDelete"];
$deleteDelay = $_POST["DeleteDelay"];
$shuffle = $_POST['Shuffle'];
$response = null;

if($user != null && $user != ""){
	if($folder != null && $folder != ""
		&& $delay != null && $delay != ""
		&& $gap != null && $gap != ""
		&& $willDelete != null && $gap != ""
		&& $deleteDelay != null && $deleteDelay != ""){

		$now = time();
		$sql = '';
		$count = 0;
		$gap *= 60;
		$delay *= 60;
		$now += $delay;
		$connection = ConnectToDB::connect();
		$pictures = null;
		if($willDelete == "true") $willDelete = 1;

		//Get pages in folder
		$sql = 'SELECT * FROM PageFolderContents WHERE FolderID='.$folder;
		$pages = $connection->query($sql);

		//Get Pictures in folder

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.hillandclark.com/pow3rmonk3y/_include/fileUpload/server/php/?UserID=".$user->getUserID()."&PageID=folders/".$folder);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$pictures = json_decode(curl_exec($ch));
		$pictures = $pictures->files;
		curl_close($ch);

		//Delete current scheduled photos for each page
		$sql = "DELETE FROM ScheduledPhotos WHERE UserID='".$user->getUserID()."' AND (";
		$count = 1;
		while($page = $pages->fetch_assoc()){
			$pageID = $page['PageID'];
			$sql .= "PageID='".$pageID."'";
			if($count != $pages->num_rows) $sql .= ' OR ';
			$count++;
		}
		$sql .= ')';
		$connection->query($sql);
		$pages->data_seek(0);

		//Generate SQL for inserting data into ScheduledPhotos
		$sql = '';
		while($page = $pages->fetch_assoc()){
			if($shuffle == "true") shuffle($pictures);
			$sql .= 'INSERT INTO ScheduledPhotos VALUES ';
			for($pic = 0; $pic < count($pictures); $pic++){
				$timeToPost = $now + ($gap * $pic);
				$url = $pictures[$pic]->url;
				$pageID = $page['PageID'];
				$pageAccessToken = $page['PageAccessToken'];
				$sql .= '(NULL, "'.$user->getUserID().'", "'.$pageID.'", "'.$url.'", "'.$timeToPost.'", "'.$pageAccessToken.'", NULL, "'.$willDelete.'", "'.$deleteDelay.'", '.$folder.')';
				if($pic != count($pictures) - 1) $sql .= ', ';	
			}
			$sql .= '; ';
		}

		//commit to database
		$connection->multi_query($sql);

		//check each statement for errors
		$response = array();
		$response['errors'] = array();
		$response['sql_error'] = false;
		do{
			if($connection->error){
				$response['sql_error'] = true;
				array_push($response['errors'], $connection->error);
			}
		}while($connection->more_results() && $connection->next_result());

		if(!$response['sql_error']){
			$response = array("success" => true);
		}

		$connection->close();
	}else{
		$response = array("error" => "invalid paremeters");
	}
}else{
	$response = array("error" => "user is not logged in");
}

echo json_encode($response);
?>