<?php

class User{
	private $session;
	
	public function __construct($session){
		$this->session = $session;
	}
	
	public function getSession(){ return $this->session; }
	public function setSession($session){ $this->session = $session; }
}

?>