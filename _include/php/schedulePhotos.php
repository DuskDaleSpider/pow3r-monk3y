<?php


require_once ( 'Facebook/Entities/AccessToken.php' );
require_once ( 'Facebook/FacebookSession.php' );
require_once ( 'Facebook/FacebookSDKException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookOtherException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookAuthorizationException.php');
require_once ( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once ( 'Facebook/HttpClients/FacebookCurl.php' );
require_once ( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( 'Facebook/FacebookRequest.php' );
require_once ( 'Facebook/FacebookResponse.php' );
require_once ( 'Facebook/GraphObject.php' );
require_once ( 'Facebook/GraphSessionInfo.php' );
require_once ( 'Facebook/FacebookServerException.php');
require_once ( 'Facebook/FacebookThrottleException.php');
require_once ( 'Facebook/FacebookRedirectLoginHelper.php');


require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/Result.php');
require_once( 'PowerMonkey/SchedulingResults.php');
require_once( 'PowerMonkey/ConnectToDB.php');

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\User;
use PowerMonkey\Result;
use PowerMonkey\SchedulingResults;
use PowerMonkey\ConnectToDB;

session_start();

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$user = $_SESSION["User"];
$page = $_POST["PageID"];
$delay = $_POST["Delay"];
$gap = $_POST["Gap"];
$willDelete = $_POST["WillDelete"];
$deleteDelay = $_POST["DeleteDelay"];
$shuffle = $_POST['Shuffle'];

if(isset($user) && isset($page) && isset($delay) && isset($gap) && isset($willDelete) && isset($deleteDelay)){
	
	$connection = ConnectToDB::connect();

	$now = time();
	$sql = '';
	$count = 0;
	$gap *= 60;
	$delay *= 60;
	$now += $delay;

	$sql = 'DELETE FROM ScheduledPhotos WHERE PageID="'.$page.'" AND UserID="'.$user->getUserID().'" AND FolderID IS NULL;';

	$connection->query($sql);

	$sql = '';

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "https://www.hillandclark.com/pow3rmonk3y/_include/fileUpload/server/php/?UserID=" . $user->getUserID() . "&PageID=" . $page);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$pictures = json_decode(curl_exec($ch));
	$pictures = $pictures->files;

	curl_close($ch);

	$pageAccessToken = '';

	try{
		$userPages = (new FacebookRequest($user->getSession(), "GET", "/me/accounts?limit=500"))->execute()->getGraphObject()->asArray();
	}catch(Exception $e){
		$_SESSION['User'] = null;
		$response = array(
			"error" => 2561
		);
		die(json_encode($response));
	}
	foreach($userPages["data"] as $temp){
		if($temp->id == $page){
			$pageAccessToken = $temp->access_token;
			break;
		}
	}

	if($shuffle == "true") shuffle($pictures);

	foreach($pictures as $file){
		if($willDelete == "true") $willDelete = 1;
		$timeToPost = $now + ($gap * $count);
		$url = $file->url;
		$sql .= 'INSERT INTO ScheduledPhotos VALUES'.
				'(NULL, "'.$user->getUserID().'", "'.$page.'", "'.$url.'", "'.$timeToPost.'", "'.$pageAccessToken.'", NULL, "'.$willDelete.'", "'.$deleteDelay.'", NULL);';
		$count++;
	}

	$connection->multi_query($sql);

	$connection->close();

	$response = new SchedulingResults([
		"message" => "All set!",
		"err" => -1,
		"errMessage" => "",
		"Delay" => $delay / 60,
		"Gap" => $gap / 60,
		"WillDelete" => $willDelete,
		"DeleteDelay" => $deleteDelay
	]);
}else{
	$response = new Result([
	 "err" => "1", "errMessage" => "Parameters not set!"
	]);
}


echo $response->getRawJSON();
?>