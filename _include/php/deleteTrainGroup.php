<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM TrainGroupAdmins WHERE TrainID='.$train.' AND UserID="'.$user->getUserID().'"';
	$result = $connection->query($sql);
	if($result->num_rows > 0){
		$sql = 'SELECT * FROM TrainGroups WHERE ID='.$train;
		$trainGroup = $connection->query($sql)->fetch_assoc();
		$sql = 'DELETE FROM TrainGroupComments WHERE TrainID='.$train.';'.
			   'DELETE FROM TrainGroupMembers WHERE TrainID='.$train.';'.
			   'DELETE FROM TrainGroupAdmins WHERE TrainID='.$train.';';
		if($trainGroup['TrainType'] == 'Page'){
			$sql .= 'DELETE FROM PageTrainLinks WHERE TrainID='.$train.';';
		}
		$sql .= 'DELETE FROM TrainGroups WHERE ID='.$train.';';

		$connection->multi_query($sql);

		$response = array("success" => true);
	}else{
		$response = array("success" => false, "error" => "User is not an admin"); 
	}
	$connection->close();
}else{
	$response = array("success" => false, "error" => "User is not logged in");
}

echo json_encode($response);

?>