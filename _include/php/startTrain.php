<?php

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	//check if user is admin of train group

	$sql = 'SELECT UserID FROM TrainGroupAdmins WHERE TrainID='.$train.' AND UserID="'.$user->getUserID().'"';
	$result = $connection->query($sql);
	if($result->num_rows > 0){
		//Get Train group info
		$sql = 'SELECT * FROM TrainGroups WHERE ID='.$train;
		$trainGroup = $connection->query($sql)->fetch_assoc();
		$now = time();
		$gap = $trainGroup['Gap'] * 60;
		$delay = $trainGroup['Delay'] * 60;
		$now = $now + $delay;
		$willDelete = $trainGroup['WillDelete'];
		$deleteDelay = $trainGroup['DeleteDelay'];

		if($trainGroup['TrainType'] == 'Page'){
			//get pages on train
			$sql = 'SELECT * FROM PageTrainLinks WHERE TrainID='.$train;
			$pagesResult = $connection->query($sql);
			if($pagesResult->num_rows > 1){
				$pages = array();
				
				while($page = $pagesResult->fetch_assoc())
					array_push($pages, $page);

				$sql = '';
				for($x = 0; $x < count($pages); $x++){
					$count = 0;
					$sql .= 'INSERT INTO ScheduledTrainLinks (UserID, PageID, Link, TimeToPost, PageAccessToken, Message, WillDelete, DeleteDelay, TrainID) VALUES ';
					$values = array();
					for($y = 0; $y < count($pages); $y++){
						if($x != $y){
							$timeToPost = $now + ($gap * $count);
							$pageLink = 'https://www.facebook.com/'.$pages[$y]["PageID"];
							$values[] = '("'.$pages[$x]["UserID"].'", "'.$pages[$x]["PageID"].'", '.
									'"'.$pageLink.'", "'.$timeToPost.'", "'.$pages[$x]["PageAccessToken"].'", '.
									'NULL, "'.$willDelete.'", "'.$deleteDelay.'", '.$train.')';
							$count++;
						}
					}
					$sql .= implode(', ', $values);
					$sql .= ';';
				}
				$connection->multi_query($sql);
				$connection->close();
				$connection = ConnectToDB::connect();
				$sql = 'SELECT UserID FROM TrainGroupAdmins WHERE TrainID='.$train.' AND UserID != "'.$user->getUserID().'"'.
						'UNION ALL '.
						'SELECT UserID FROM TrainGroupMembers WHERE TrainID='.$train;
				$members = $connection->query($sql);
				
				$sql = 'SELECT Name FROM TrainGroups WHERE ID='.$train.'; '.
					   'SELECT Name FROM UserNames WHERE UserID="'.$user->getUserID().'";';
				$connection->multi_query($sql);
				$trainGroup = $connection->store_result()->fetch_assoc();
				$connection->next_result();
				$userName = $connection->store_result()->fetch_assoc();
				$message = $connection->real_escape_string($userName['Name'].' has started the train in <a href="javascript:void(0)" onclick="PowerMonkey.showTrainGroupDetails('.$train.')">'.$trainGroup['Name'].'</a>');

				$sql = 'INSERT INTO Notifications VALUES ';
				$values = array();
				while($member = $members->fetch_assoc()){
					$userID = $member['UserID'];
					$values[] = '(NULL, "'.$userID.'", "'.$message.'", 0, '.$now.')';
				}
				$sql .= implode(',', $values).';';
				$connection->query($sql);
				
				$response = array("success" => true);
			}else{
				$response = array("success" => false, "error" => "not enough pages");
			}
		}

	}else{ //user is not an admin
		$response = array("success" => false, "error" => "User is not an admin");
	}
	$connection->close();
}else{
	$response = array("success" => false, "error" => "User is not logged in!");
}

echo json_encode($response);

?>