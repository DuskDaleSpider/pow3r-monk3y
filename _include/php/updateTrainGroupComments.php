<?

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$lastChecked = $_GET['LastCheck'];
$train = $_GET['Train'];
$response = null;
$now = time();

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'SELECT UserID FROM TrainGroupAdmins WHERE TrainID='.$train.';'.
			'SELECT UserID FROM TrainGroupMembers WHERE TrainID='.$train.';';
	$sql .= 'SELECT tgc.ID, tgc.Comment, tgc.TimePosted, tgc.UserID, un.Name '.
			'FROM TrainGroupComments AS tgc '.
			'JOIN UserNames AS un ON tgc.UserID=un.UserID '.
			'WHERE TrainID='.$train.' AND TimePosted > '.$lastChecked.';';
	$connection->multi_query($sql);

	$members = array();
	$admins = array();

	//get data for admins
	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		die(json_encode($response));
	}else{
		$trainAdmins = $connection->store_result();
		while($admin = $trainAdmins->fetch_assoc()){
			array_push($admins, $admin['UserID']);
		}
	}
	
	//move onto members
	$connection->next_result();
	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		die(json_encode($response));
	}else{
		$trainMembers = $connection->store_result();
		while($member = $trainMembers->fetch_assoc()){
			array_push($members, $member['UserID']);
		}
	}

	//move onto comments
	$connection->next_result();
	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		die(json_encode($response));
	}else{
		$comments = $connection->store_result();
		$html = '';
		while($comment = $comments->fetch_assoc()){
			$userType = "";
			if(in_array($comment['UserID'], $admins)){
				$userType = "admin";
			}else if(in_array($comment['UserID'], $members)){
				$userType = "member";
			}

			$html .= '<li>'.
						'<span class="username '.$userType.'">'.$comment['Name'].'</span>: '.$comment['Comment'].
					'</li>';
		}
		$response = array("success" => true, "lastCheck" => $now, "data" => $html);
	}
	$connection->close();
	echo json_encode($response);
}