<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

//include delete_dir function
include('PowerMonkey/DeleteDirectory.php');

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$response = null;

if($user != null && $user != ""){
	if($folder != null && $folder != ""){
		$connection = ConnectToDB::connect();

		//delete folder data from database
		$sql = 'DELETE FROM PageFolderContents WHERE FolderID='.$folder.';' 
			  .'DELETE FROM PageFolders WHERE ID='.$folder.';'
			  .'DELETE FROM ScheduledPhotos WHERE FolderID='.$folder.';'
			  .'DELETE FROM ScheduledStatus WHERE FolderID='.$folder.';'
			  .'DELETE FROM ScheduledLinks WHERE FolderID='.$folder.';'
			  .'DELETE FROM FolderStatus WHERE FolderID='.$folder.';';
		$connection->multi_query($sql);
		if($connection->error){
			$response = array(
				"error" => $connection->error
			);
			die(json_encode($response));
		}
		$connection->close();

		//delete folder photo directory
		try{
			delete_dir('../fileUpload/server/php/files/'.$user->getUserID().'/folders/'.$folder.'/');
		}catch(Exception $e){
		}

		$response = array(
			"success" => true
		);
	}else{
		$response = array(
			"error" => "invalid parameters"
		);
	}
}else{
	$response = array(
		"error" => "user is logged out"
	);
}

echo json_encode($response);
?>