<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$id = $_POST['ID'];
$response = null;

if($user != null){
	$connection = ConnectToDB::connect();
	$sql = 'DELETE FROM Notifications WHERE ID='.$id;
	$results = $connection->query($sql);
	$connection->close();
	$response = array("success" => true);
}else{
	$response = array("success" => false, "error" => "User is not logged in");
}

echo json_encode($response);

?>