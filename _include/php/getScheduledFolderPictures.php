<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_GET['Folder'];
$response = null;

if($user != null && $user != ""){
	if($folder != null && $folder != ""){
		$connection = ConnectToDB::connect();
		$sql = 'SELECT * FROM ScheduledPhotos WHERE FolderID='.$folder.' GROUP BY PicLink ORDER BY TimeToPost ASC;'. 
				'SELECT * FROM ScheduledDeletes WHERE FolderID='.$folder.' GROUP BY TimeToDelete ORDER BY TimeToDelete ASC;';
		$connection->multi_query($sql);
		if($connection->error){
			$response = array("error" => $connection->error);
			die(json_encode($response));
		}else{
			$response = array(
				"scheduledPhotos" => array(),
				"scheduledDeletes" => array()
			);
			//retrieve the scheduled photos
			$scheduledPhotos = $connection->store_result();
			while($photo = $scheduledPhotos->fetch_assoc()){
				array_push($response['scheduledPhotos'], $photo);
			}
			//retrieve the scheduled deletes
			$connection->next_result();
			if($connection->error){
				$response['sql_error'] = true;
				$response['errors'] = array($connection->error);
			}else{
				$scheduledDeletes = $connection->store_result();
				while($delete = $scheduledDeletes->fetch_assoc()){
					array_push($response['scheduledDeletes'], $delete);
				}
			}
		}
		$connection->close();
	}else{
		$response = array("error" => "invalid parameters");
	}
}else{
	$response = array("error" => "user is not logged in");
}

echo json_encode($response);
?>