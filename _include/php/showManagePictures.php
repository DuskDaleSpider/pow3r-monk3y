<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');
require_once('PowerMonkey/Result.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;
use PowerMonkey\Result;

session_start();

$user = $_SESSION["User"];
$pageID = $_GET["PageID"];
$now = time();
$scheduledPhotos = array();
$scheduledDeletes = array();
$response = null;

if($user != NULL && $user != "" && $pageID != null && $pageID != ""){
	$connection = ConnectToDB::connect();

	$sql = "SELECT * FROM ScheduledPhotos WHERE UserID=" . $user->getUserID() . " AND PageID=". $pageID . " AND TimeToPost > " . $now . ' AND FolderID IS NULL'; 
	$results = $connection->query($sql);

	if($connection->error){
		$response = new Result([
			"err" => 1,
			"errMessage" => $connection->error
		]);
		die($response->getRawJSON());
	}

	while($row = $results->fetch_assoc()){
		$scheduledPhotos[] = $row;
	}

	$results->close();

	//retrieve scheduled delete for users page
	$sql = 'SELECT * FROM ScheduledDeletes WHERE UserID="' . $user->getUserID() . '" AND PageID="'.$pageID.'" AND TimeToDelete > '.$now.';';
	$results = $connection->query($sql);

	if($connection->error){
		$response = new Result([
			"err" => 1,
			"errMessage" => $connection->error
		]);
		die($response->getRawJSON());
	}

	while($row = $results->fetch_assoc()){
		$scheduledDeletes[] = $row;
	}

	$results->close();
	$connection->close();

	$response = '{'.
					'"scheduledPhotos": ' . json_encode($scheduledPhotos). ','.
					'"scheduledDeletes": ' . json_encode($scheduledDeletes).
				'}';

	echo $response;
}else{
	$response = new Result([
		"err" => 1,
		"errMessage" => "Parameters were invalid"
	]);
	die($response->getRawJSON());
}

?>