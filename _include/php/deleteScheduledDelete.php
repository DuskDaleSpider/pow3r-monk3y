<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');
require_once('PowerMonkey/Result.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;
use PowerMonkey\Result;

session_start();

$user = $_SESSION["User"];
$ID = $_POST["ID"];
$response = null;

if($user != null && $user != ""){
	if($ID != null && $ID != ""){
		$connection = ConnectToDB::connect();
		$sql = "DELETE FROM ScheduledDeletes WHERE ID=" . $ID;
		$result = $connection->query($sql);
		if($connection->error){
			$response = new Result([
					"err" => 1,
					"errMessage" => $connection->error
				]);
		}else{
			$response = new Result(["message" => "Scheduled delete stopped successfully"]);
		}
	}else{
		$response = new Result([
				"err" => 1,
				"errMessage" => "Parameters not valid"
			]);
	}
}else{
	$response = new Result([
			"err" => 1,
			"errMessage" => "User is not valid"
		]);
}

echo $response->getRawJSON();


?>