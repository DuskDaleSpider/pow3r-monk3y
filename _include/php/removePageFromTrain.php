<?php

require_once ('PowerMonkey/User.php');
require_once ('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_POST['Train'];
$page = $_POST['Page'];
$rowID = $_POST['Row'];
$response = null;

if($user != null && $user != ""){
	if($train != null && $train != "" && $page != null && $page != ""){
		$connection = ConnectToDB::connect();
		try{
			$sql = 'SELECT Admin FROM Trains WHERE ID='.$train;
			$admin = $connection->query($sql)->fetch_assoc();
			if($admin['Admin'] == $user->getUserID()){
				$sql = 'DELETE FROM TrainLinks WHERE ID='.$rowID;
				$connection->query($sql);

				$connection->close();
				$response = array(
					"success" => true
				);
			}else{
				$sql = 'DELETE FROM TrainLinks WHERE UserID="'.$user->getUserID().'" AND PageID="'.$page.'" AND TrainID='.$train;
				$connection->query($sql);
				if($connection->affected_rows > 0){
					$response = array(
						"success" => true
					);
				}else{
					$response = array(
						"error" => "You do not have permission to remove that page."
					);
				}
			}
		}catch(Exception $e){
			$_SESSION['User'] = null;
			$response = array(
				"error" => "You are not logged in. Reloading..",
				"errorNum" => 100
			);
		}
	}else{
		$response = array(
			"error" => "Something went wrong, please try again."
		);
	}
}else{
	$response = array(
		"error" => "You are not logged in. Reloading..",
		"errorNum" => 100
	);
}

if($response["success"] === true){
	$connection = ConnectToDB::connect();
	$sql = 'SELECT COUNT(*) AS Total FROM TrainLinks WHERE TrainID='.$train;
	$total = $connection->query($sql)->fetch_assoc();
	$sql = 'UPDATE Trains SET NumRiders='.$total['Total'].' WHERE ID='.$train;
	$connection->query($sql);
	$connection->close();
}

echo json_encode($response);
?>