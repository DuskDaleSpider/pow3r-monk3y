<?php

require_once('PowerMonkey/User.php');

use PowerMonkey\User;

session_start();

$user = $_SESSION['User'];
$folder = $_GET['Folder'];
$response = null;

if($user != null && $user != ""){

	if($folder != null && $folder != ""){

		$filepath = '../fileUpload/server/php/files/'.$user->getUserID().'/links/folders/'.$folder.'.dat';
		fclose(fopen($filepath, 'w'));
		$response = array(
			'success' => true
		);
	}else{
		$response = array(
			'error' => 'Invalid parameters'
		);
	}

}else{
	$response = array(
		'error' => 'User is not logged in'
	);
}

echo json_encode($response);

?>