<?php

require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$page = $_GET['Page'];
$response = null;

if($user != null && $user != ""){

	if($page != null && $page != ""){

		$connection = ConnectToDB::connect();
		$sql = 'DELETE FROM PageStatus WHERE PageID="'.$page.'"';
		$connection->query($sql);
		$connection->close();
		$response = array(
			'success' => true
		);
	}else{
		$response = array(
			'error' => 'Invalid parameters'
		);
	}

}else{
	$response = array(
		'error' => 'User is not logged in'
	);
}

echo json_encode($response);

?>