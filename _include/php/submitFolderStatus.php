<?php

require_once( 'PowerMonkey/User.php' );
require_once( 'PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_POST['Folder'];
$status = $_POST['Status'];
$response = array();

if($user != null && $user != ""){
	for($i = 0; $i < count($status); $i++){
		if($status[$i] == "") unset($status[$i]);
	} 
	$status = array_values($status);
	
	$connection = ConnectToDB::connect();
	$sql = 'INSERT INTO FolderStatus VALUES ';
	for($i = 0; $i < count($status); $i++){
		$sql .= '(NULL, '.$folder.', "'.$connection->real_escape_string($status[$i]).'")';
		if($i != count($status) - 1) $sql .= ', ';
	}
	$connection->query($sql);
	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
	}else{
		$response = array("success" => true, "message" => "Status has been added!");
	}	
	$connection->close();
}

echo json_encode($response);

?>