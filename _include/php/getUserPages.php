<?php

require_once ( 'Facebook/Entities/AccessToken.php' );
require_once ( 'Facebook/FacebookSession.php' );
require_once ( 'Facebook/FacebookSDKException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookOtherException.php');
require_once ( 'Facebook/FacebookRequestException.php');
require_once ( 'Facebook/FacebookAuthorizationException.php');
require_once ( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once ( 'Facebook/HttpClients/FacebookCurl.php' );
require_once ( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( 'Facebook/FacebookRequest.php' );
require_once ( 'Facebook/FacebookResponse.php' );
require_once ( 'Facebook/GraphObject.php' );
require_once ( 'Facebook/GraphSessionInfo.php' );
require_once ( 'Facebook/FacebookServerException.php');
require_once ( 'Facebook/FacebookThrottleException.php');
require_once ( 'Facebook/FacebookRedirectLoginHelper.php');


require_once('PowerMonkey/User.php');
require_once('PowerMonkey/ConnectToDB.php');

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

$user = $_SESSION['User'];
$response = null;

if($user != null && $user != ""){
	$connection = ConnectToDB::connect();
	$sql = "SELECT DISTINCT pfc.PageID ".
			"FROM PageFolders ".
				"INNER JOIN PageFolderContents as pfc on PageFolders.ID=pfc.FolderID ".
			"WHERE UserID='".$user->getUserID()."' ".
			"LIMIT 500";
	$groupedPages = $connection->query($sql);


	if($connection->error){
		$response = array(
			"success" => false,
			"error" => $connection->error
		);
		die(json_encode($response));
	}

	$userPages = (new FacebookRequest($user->getSession(), 'GET', '/me/accounts?limit=500'))->execute()->getGraphObject()->asArray();
	$userPages = $userPages['data'];

	//get more data for user pages
	for($i = 0; $i < count($userPages); $i++){
		$pageToken = $userPages[$i]->access_token;
		$userPages[$i]->isVisible = true;
		$userPages[$i]->token = $pageToken;
	}

	$connection->close();

	$response = array(
		"success" => true,
		"data" => $userPages
	);
}else{
	$response = array(
		"success" => false,
		"error" => "User Not Logged In!"
	);
}

echo json_encode($response);
?>