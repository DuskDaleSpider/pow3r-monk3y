//creates a cookie
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//reads a cookie
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//erases a cookie
function eraseCookie(name) {
	createCookie(name,"",-1);
}

jQuery(function($){
	var PowerMonkey = window.PowerMonkey || {};
	
	PowerMonkey.userPages = null;

	PowerMonkey.userFolders = null;

	PowerMonkey.pagesProfilePic = null;

	PowerMonkey.slides = null;

	PowerMonkey.pageThumbs = "";

	PowerMonkey.numPages = 0;

	PowerMonkey.numProfilePicsAdded = 0;

	PowerMonkey.currentIndex = 0;

	PowerMonkey.currentManagePictures = null;

	PowerMonkey.username = "";

	PowerMonkey.userCoverURL = "";

	PowerMonkey.managePhotosInterval = null;

	PowerMonkey.keyListener = null;

	PowerMonkey.pageTokens = null;

	PowerMonkey.currentTrain = null;

	PowerMonkey.currentFolder = null;

	PowerMonkey.FOLDER_SELECTIONS = {
		PHOTOS: 0,
		LINKS: 1,
		STATUS: 2,
		VIDEOS: 3
	};

	PowerMonkey.pagesLoaded = 0;

	PowerMonkey.commentsLastChecked = 0;

	PowerMonkey.numNotifications = 0;

	PowerMonkey.notiLastChecked = 0;

	PowerMonkey.notificationInterval = null;

	PowerMonkey.currentTrainGroup = 0;

	PowerMonkey.trainGroupInterval = null;

	PowerMonkey.checkUser = function(){
		$.ajax({
			url: '_include/php/CheckUser.php?User='+userId+"&AccessToken="+FB.getAccessToken()
		}).done(function(response){
			response = JSON.parse(response);
			if(response.error < 0){
				console.log(response.message);
				FB.setAccessToken(response.token);
			}else{
				console.log(response.errorMessage);
			}
		});
	}

	PowerMonkey.getPagesProfilePics = function(){
		PowerMonkey.pagesProfilePic = [];
		PowerMonkey.numProfilePicsAdded = 0;
		for(var pageCount = 0; pageCount < PowerMonkey.userPages.length; pageCount++){
			$.ajax({
				url: '_include/php/getProfilePicture.php',
				method: 'POST',
				data: {
					Page: PowerMonkey.userPages[pageCount].id
				}
			}).done(function(resp){
				resp = JSON.parse(resp);
				PowerMonkey.updateProgress();
				for(var i = 0; i < PowerMonkey.userPages.length; i++){
					if(PowerMonkey.userPages[i].id == resp.id){
							PowerMonkey.userPages[i].profilePic = resp.url;
							PowerMonkey.numProfilePicsAdded++;
					}
					if(PowerMonkey.numProfilePicsAdded == PowerMonkey.userPages.length){
						PowerMonkey.showPageThumbs();
						/*
						$.fancybox.close();
						document.getElementById("jSplash").remove();
						$("body").attr('style', 'overflow: auto');
						*/
					}
				}
			});
		}
	}

	PowerMonkey.updateProgress = function(){
		PowerMonkey.pagesLoaded++;
		var progress = parseInt((PowerMonkey.pagesLoaded / PowerMonkey.userPages.length) * 100);
		document.querySelector(".progress-bar").setAttribute("style", "width: "+progress+"%;");
		document.querySelector(".progress-bar .sr-only").innerHTML = progress + "%";
	}

	PowerMonkey.compare = function(a,b) {
	  if (a.name < b.name)
	    return -1;
	  if (a.name > b.name)
	    return 1;
	  return 0;
	}

	PowerMonkey.replaceUserPage = function(data){

		PowerMonkey.userPages.push(data);

		if(PowerMonkey.userPages.length == PowerMonkey.numPages){
				PowerMonkey.userPages.sort(PowerMonkey.compare);
				console.log("Starting check of page profile pics");
				PowerMonkey.getPagesProfilePics();
		}

	}

	PowerMonkey.getPageAccessTokens = function(){
		$.ajax({
			url: '_include/php/getPageAccessTokens.php'
		}).done(function(response){
			response = JSON.parse(response);
			userPages = PowerMonkey.userPages;
			for(var i = 0; i < response.length; i++){
				for(var x = 0; x < userPages.length; x++){
					if(response[i].id == userPages[x].id){
						userPages[x].token = response[i].access_token;
						break;
					}
				}
			}
			PowerMonkey.userPages = userPages;
		});
	}

	PowerMonkey.showPageThumbs = function(){
		$("#supersized img").attr("src", PowerMonkey.userCoverURL);
		$(".title-page .title").html(PowerMonkey.username);
		$(".type-work a").html("Choose Page");
		$(".type-work a").attr('onclick', 'PowerMonkey.showPageThumbs()');

		document.getElementById("projects").innerHTML = "<ul id=\"thumbs\"><ul>";

		document.getElementById("thumbs").innerHTML = "";

		var elements = document.querySelectorAll("#filters li");

		for(var i = 1; i < elements.length; i++){
			$(elements[i]).animate({top: '-100px', opacity: 0});
		}

		for(var i = 0; i < PowerMonkey.userPages.length; i++){
			if(typeof PowerMonkey.userPages[i].isVisible != "undefined" && PowerMonkey.userPages[i].isVisible){
				document.getElementById("thumbs").innerHTML += '<li class="item-thumbs col-sm-3" data-id="'+PowerMonkey.userPages[i].id+'">'+
	            	'<a class="hover-wrap" data-fancybox-group="gallery" title="'+PowerMonkey.userPages[i].name+'">'+
	                	'<section class="page-title-wrapper">'+
	                		'<span class="overlay-img">'+
	                			'<section class="page-name">'+
	                				PowerMonkey.userPages[i].name+
	                			'</section>'+
	                		'</span>'+
	                    '</section>'+
	                    '<span class="overlay-img-thumb"></span>'+
	                '</a>'+
	                '<img src="'+PowerMonkey.userPages[i].profilePic+'" alt="'+PowerMonkey.userPages[i].name+'">'+
	            '</li>';
        	}
		}


		$("#thumbs li").each(function(index){
			$(this).click(function(){
				var clickedPage = $(this).attr('data-id');

				index = null;
				for(var i = 0; i < PowerMonkey.userPages.length; i++){
					if(clickedPage == PowerMonkey.userPages[i].id){
						index = i;
						break;
					}
				}
				PowerMonkey.currentIndex = index;
				
				PowerMonkey.showPageActivity();

				PowerMonkey.changeInfo(index);
		
				var elements = document.querySelectorAll("#filters a");

				for(var i = 1; i < elements.length; i++){
					$(elements[i].parentElement).animate({top: '0px', opacity: 1});

				}

				elements[0].setAttribute("class", "");
				elements[1].setAttribute("class", "selected");
				elements[1].setAttribute("onclick", "PowerMonkey.changePages("+index+")");
			});
		});
	}

	PowerMonkey.changeInfo = function(index){
		var coverPhotoURL = "https://www.hillandclark.com/pow3rmonk3y/_include/img/cover.jpg";
		try{
			coverPhotoURL = PowerMonkey.userPages[index].cover.source;
		}catch(err){
			FB.api('/'+PowerMonkey.userPages[index].id+'?fields=cover', function(data){
				PowerMonkey.userPages[index].cover = data.cover;
				PowerMonkey.changeInfo(index);
			});
		}
		var pageName = PowerMonkey.userPages[index].name;
		$("#supersized img").attr("src", coverPhotoURL);
		$(".title-page .title").html(pageName);
	}

	PowerMonkey.changePages = function(index){
		PowerMonkey.currentIndex = index;
		tmpl.cache['template-upload'] = null;
		tmpl.cache['template-download'] = null;
		$.ajax({
			url: "_include/views/photo-uploader.php?userID="+userId+"&pageID="+PowerMonkey.userPages[index].id
		}).done(function(data){
			document.getElementById("projects").innerHTML = data;
			// Initialize the jQuery File Upload widget:
			$('#fileupload').fileupload({
			    // Uncomment the following to send cross-domain cookies:
			    //xhrFields: {withCredentials: true},
			    url: "_include/fileUpload/server/php/?UserID="+userId+"&PageID="+PowerMonkey.userPages[index].id,
			    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
			}).bind('fileuploadprogressall', function (e, data) {
			    var progress = parseInt(data.loaded / data.total * 100, 10);
				$("#progressAllBar").attr({style: "width: "+progress+"%"});
				$("#progressAllBar span").html(progress+"%");
			});

			// Enable iframe cross-domain access via redirect option:
			$('#fileupload').fileupload(
			    'option',
			    'redirect',
			    window.location.href.replace(
			        /\/[^\/]*$/,
			        '/cors/result.html?%s'
			    )
			);

			// Load existing files:
			$('#fileupload').addClass('fileupload-processing');
			$.ajax({
			    // Uncomment the following to send cross-domain cookies:
			    //xhrFields: {withCredentials: true},
			    url: $('#fileupload').fileupload('option', 'url'),
			    dataType: 'json',
			    context: $('#fileupload')[0]
		    }).always(function () {
		        $(this).removeClass('fileupload-processing');
		    }).done(function (result) {
		        $(this).fileupload('option', 'done')
		   	     .call(this, $.Event('done'), {result: result});
			});
		});
	}

	PowerMonkey.replacePagePicture = function(data, index){
		data = data.data;
		if(typeof data.url != "undefined"){
			PowerMonkey.pagesProfilePic[index] = data.url;
			PowerMonkey.numProfilePicsAdded++;
		}else{
			console.log("Facebook Error! : ");
			console.log(data);
		}
		if(PowerMonkey.numProfilePicsAdded == PowerMonkey.numPages){
			console.log("Setting pagesProfilePic in local storage! :D");
			localStorage.setItem("pagesProfilePic", JSON.stringify(PowerMonkey.pagesProfilePic));
			console.log(JSON.parse(localStorage.getItem("pagesProfilePic")));
			PowerMonkey.showPageThumbs();
		}
	}
	
	PowerMonkey.getUserPages = function(){
			$.ajax({
				url: '_include/php/getUserPages.php'
			}).done(function(data){
				data = JSON.parse(data);
				PowerMonkey.userPages = data.data;
				PowerMonkey.numPages = data.data.length;

				for(var i = 0; i < PowerMonkey.userPages.length; i++){
					PowerMonkey.userPages[i].token = PowerMonkey.userPages[i].access_token;
					PowerMonkey.userPages[i].isVisible = true;
				}
				PowerMonkey.userPages.sort(PowerMonkey.compare);
				PowerMonkey.getPagesProfilePics();

				$.ajax({
					url: '_include/php/setUserPages.php',
					method: 'post',
					data : {
						Pages: PowerMonkey.userPages
					}
				}).done(function(data){
					data = JSON.parse(data);
					if(!data.success){
						console.log(data.error);
					}
				});
			});
	};

	PowerMonkey.setUserPages = function(){
		$.ajax({
			url: '_include/php/getPagesInGroups.php'
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				for(var i = 0; i < PowerMonkey.userPages.length; i++){
					if(data.pages.indexOf(PowerMonkey.userPages[i].id) > 0){
						PowerMonkey.userPages[i].isVisible = false;
					}else{
						PowerMonkey.userPages[i].isVisible = true;
					}
				}
			}
		});
	}

	//start slides
	PowerMonkey.startSlider = function(){
		$.supersized({
			// Functionality
			slideshow               :   1,			// Slideshow on/off
			autoplay				:	1,			// Slideshow starts playing automatically
			start_slide             :   1,			// Start slide (0 is random)
			stop_loop				:	0,			// Pauses slideshow on last slide
			random					: 	0,			// Randomize slide order (Ignores start slide)
			slide_interval          :   2000,		// Length between transitions
			transition              :   5, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
			transition_speed		:	300,		// Speed of transition
			new_window				:	1,			// Image links open in new window/tab
			pause_hover             :   0,			// Pause slideshow on hover
			keyboard_nav            :   1,			// Keyboard navigation on/off
			performance				:	1,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
			image_protect			:	1,			// Disables image dragging and right click with Javascript
													   
			// Size & Position						   
			min_width		        :   0,			// Min width allowed (in pixels)
			min_height		        :   0,			// Min height allowed (in pixels)
			vertical_center         :   1,			// Vertically center background
			horizontal_center       :   1,			// Horizontally center background
			fit_always				:	1,			// Image will never exceed browser width or height (Ignores min. dimensions)
			fit_portrait         	:   0,			// Portrait images will not exceed browser height
			fit_landscape			:   0,			// Landscape images will not exceed browser width
													   
			// Components							
			slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
			thumb_links				:	0,			// Individual thumb links for each slide
			thumbnail_navigation    :   0,			// Thumbnail navigation
			slides 					:   slides,
										
			// Theme Options			   
			progress_bar			:	0,			// Timer for each slide							
			mouse_scrub				:	0
			
		});
	}

	PowerMonkey.showScheduler = function(){
		console.log("Need to show scheduler for user: " + userId + " and page: "+ PowerMonkey.userPages[PowerMonkey.currentIndex].id);
		$.ajax({
			url: '_include/views/photo-scheduler.php'
		}).done(function(response){
			document.getElementById("projects").innerHTML = response;
			
			var checkBoxes = $(".deleteInput input");

			if(checkBoxes.length > 0){
			  $(checkBoxes.get(0)).click(function(){
			    if(checkBoxes.get(0).checked){
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("hidden")[0]
			       .setAttribute("class", "visible");
			    }else{
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("visible")[0]
			       .setAttribute("class", "hidden");
			    }
			  });
			}

			$('.toggle').toggles();
		});
	}

	PowerMonkey.schedulePhotos = function(){
		var currentPage = PowerMonkey.userPages[PowerMonkey.currentIndex].id;

		var delay = parseInt($(".delayInput select").get(0).value);
		var isError = false;
		if(delay.toString() == "NaN"){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(delay < 0){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a positive number";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("delayInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var gap = parseInt($(".gapInput select").get(0).value);
		if(gap.toString() == "NaN"){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(gap < 1){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("gapInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var willDelete = $(".deleteInput input").get(0).checked;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
			if(deleteDelay.toString() == "NaN"){
				$(".deleteInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number!";
				isError = true;
			}else if(deleteDelay < 1){
				$(".deleteInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
				isError = true;
			}
		}

		if(isError){
			document.getElementsByClassName("deleteInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}		

		var shuffle = $('.toggle').data('toggles').active;

		$.ajax({
			url: "_include/php/schedulePhotos.php",
			method: "POST",
			data: {
				PageID: currentPage,
				Delay: delay,
				Gap: gap,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay,
				Shuffle: shuffle
			}
		}).done(function(response){
			var results = JSON.parse(response);
			console.log(results);
			if(results.error == -1){
				var delay = results.Delay;
				var gap = results.Gap;
				var willDelete = results.WillDelete;
				var DeleteDelay = results.DeleteDelay;
				var message = '<h1 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">Your photos have been scheduled to start in ' + delay+
					' minute(s), post every '+gap+
					' minute(s), and ';
				if(willDelete == "1"){
					message += ' will delete after ' + DeleteDelay + ' minute(s).';
				}else{
					message += ' will not be deleted.';
				}
				message+= "</h1>";
				$.fancybox.open(message);
			}else if(results.error == 2561){
				$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
									'You are not logged in. Reloading page..'+
								'</h3>');
				location.reload();
			}
		});
	}

	//TODO: Add filter for isotope that selects between posts that are planned to post and the onesthat are planned to delete

	PowerMonkey.showManagePictures = function(){
		$.ajax({
			url: '_include/php/showManagePictures.php?PageID=' + PowerMonkey.userPages[PowerMonkey.currentIndex].id
		}).done(function(response){
			response = JSON.parse(response);
			PowerMonkey.currentManagePictures = response;
			if(typeof response.err == "undefined"){
				document.getElementById("projects").innerHTML = "<ul id=\"thumbs\" class=\"managePictures\"></ul>";
				var scheduledPhotos = response.scheduledPhotos;
				var scheduledDeletes = response.scheduledDeletes;
				for(var i = 0; i < scheduledPhotos.length; i++){
					document.getElementById("thumbs").innerHTML += '<li class="item-thumbs col-sm-3">' +
						'<a class="hover-wrap fancybox-media" data-fancybox-group="gallery" href="'+scheduledPhotos[i].PicLink+'" title="Hey There">'+
							'<section class="page-title-wrapper">'+
								'<span class="overlay-img">'+
									'<section class="page-name">'+
									'</section>'+
								'</span>'+
							'</section>'+
							'<span class="overlay-img-thumb font-icon-plus"></span>'+
						'</a>'+
						'<img src="' + scheduledPhotos[i].PicLink + '" alt="Hey There"/>'+
					'</li>';
				}
				for(var i = 0; i < scheduledDeletes.length; i++){
					FB.api('/'+scheduledDeletes[i].PostID, function(data){
						document.getElementById("thumbs").innerHTML += '<li class="item-thumbs col-sm-3">' +
							'<a class="hover-wrap scheduledDelete" data-fancybox-group="gallery" href="'+data.source+'" title="Hey There">'+
								'<section class="page-title-wrapper">'+
									'<span class="overlay-img">'+
										'<section class="page-name">'+
										'</section>'+
									'</span>'+
								'</section>'+
								'<span class="overlay-img-thumb font-icon-minus"></span>'+
							'</a>'+
							'<img src="' + data.source + '" alt="Hey There"/>'+
						'</li>';
						});
				}

				$(".fancybox-media").fancybox({
					openEffect: 'none',
					closeEffect: 'none',
					padding: 0,
					beforeShow: function(){
						var now = Math.floor((new Date).getTime()/1000);
						var timeToPost = parseInt(PowerMonkey.currentManagePictures.scheduledPhotos[this.index].TimeToPost);
						var secToPost = timeToPost - now;
						var minToPost = ((timeToPost - now) / 60).toFixed(2);

						/*if(secToPost < 0){
							PowerMonkey.currentManagePictures.scheduledPhotos.splice(this.index, 1);
							document.getElementsByClassName("item-thumbs")[this.index].remove();
							return;
						}*/

						if(isNaN(timeToPost))
						{
							console.log("Time to post was not a number!");
							return;
						}



						this.title = '<div class="timeToPost">';
						if(minToPost <= 1){
							this.title += '<span> Will post in ' + secToPost + ' seconds';
						}else{
							this.title += '<span> Will post in ' + minToPost + ' minutes. </span>';
						} 
						this.title += '</div>';
						var message = PowerMonkey.currentManagePictures.scheduledPhotos[this.index].Message;
						this.title += '<div class="pictureMessage">';
						if(message != null && message != ""){
							this.title += '<span>' + message + '</span>';
						}else{
							this.title += '<span> No Message </span>';
						}
						this.title += '<button class="btn btn-default" onclick="PowerMonkey.editPictureMessage('+PowerMonkey.currentManagePictures.scheduledPhotos[this.index].ID+')">Edit</button>';
						this.title += '<button class="btn btn-default" onclick="PowerMonkey.editAllPictureMessages();">Edit All</button>'
						this.title += '</div>';
						this.title += '<button class="btn btn-danger" onclick="PowerMonkey.deleteScheduledPicture('+PowerMonkey.currentManagePictures.scheduledPhotos[this.index].ID+')">Delete</button>';
					},
					helper: {
						title: {
							type: 'inside'
						}
					}
				});

				$(".scheduledDelete").fancybox({
					padding: 0,
					beforeShow: function(){
						var now = Math.floor((new Date).getTime()/1000);
						var timeToDelete = parseInt(PowerMonkey.currentManagePictures.scheduledDeletes[this.index].TimeToDelete);
						var secToPost = timeToDelete - now;
						var minToPost = ((timeToDelete - now) / 60).toFixed(2);

						if(secToPost < 0){
							PowerMonkey.currentManagePictures.scheduledDeletes.splice(this.index, 1);
							document.getElementsByClassName("scheduledDelete")[0].remove();
							$.fancybox.cancel();
							return;
						}

						if(isNaN(timeToDelete))
						{
							console.log("Time to Delete was not a number!");
							return;
						}



						this.title = '<div class="timeToPost">';
						if(minToPost <= 1){
							this.title += '<span> Will delete in ' + secToPost + ' seconds';
						}else{
							this.title += '<span> Will delete in ' + minToPost + ' minutes. </span>';
						} 
						this.title += '</div>';
						this.title += '<button class="btn btn-danger" onclick="PowerMonkey.deleteScheduledDelete('+PowerMonkey.currentManagePictures.scheduledDeletes[this.index].ID+')">Stop Delete</button>';
					},
					helper: {
						title: {
							type: 'inside'
						}
					}
				});
				
				if(PowerMonkey.managePhotosInterval != null) clearInterval(PowerMonkey.managePhotosInterval);
				PowerMonkey.managePhotosInterval = setInterval(PowerMonkey.removeExpiredPosts, 1000);
			}

		});
	}

	PowerMonkey.editPictureMessage = function(id){
		PowerMonkey.stopSecret();
		$.fancybox('<h4>Message:</h4><input type="text" id="messageTextBox" autofocus onkeydown="PowerMonkey.onEnter(event, PowerMonkey.submitPictureMessage, '+id+')"/><button class="btn btn-default" onclick="PowerMonkey.submitPictureMessage('+id+')">Submit</button>');
	}

	PowerMonkey.editAllPictureMessages = function(){
		$.fancybox('<h4>Message:</h4><input type="text" id="messageTextBox" autofocus onkeydown="PowerMonkey.onEnter(event, PowerMonkey.submitPictureMessageToAll);"/><button class="btn btn-default" onclick="PowerMonkey.submitPictureMessageToAll()">Submit</button>');
	}

	PowerMonkey.submitPictureMessageToAll = function(){
		var page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		var message = document.getElementById("messageTextBox").value.trim();
		if(message != ""){
			if($.fancybox.isActive) $.fancybox.close();

			$.ajax({
				url: '_include/php/submitPictureMessageToAll.php',
				method: 'POST',
				data: {
					Page: page,
					Message: message
				}
			}).done(function(response){
				response = JSON.parse(response);
				if(response.success){
					for(var x = 0; x < PowerMonkey.currentManagePictures.scheduledPhotos.length; x++){
						PowerMonkey.currentManagePictures.scheduledPhotos[x].Message = message;
					}
					$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">' +
										'The Message has been changed to all photos successfully!' +
									'</h3>');
				}
			});
		}
	}

	PowerMonkey.submitPictureMessage = function(id){
		PowerMonkey.startSecret();
		var message = document.getElementById("messageTextBox").value;
		if(message != ""){
			$.ajax({
				url: '_include/php/submitPictureMessage.php',
				method: "POST",
				data: {
					ID: id,
					Message: message 
				}
			}).done(function(response){
				response = JSON.parse(response);
				if(response.error >= 0){
					console.log(response.errorMessage);
				}else{
					var currentPhotos = PowerMonkey.currentManagePictures.scheduledPhotos;
					for(var x = 0; x < currentPhotos.length; x++){
						if(currentPhotos[x].ID == id){
							PowerMonkey.currentManagePictures.scheduledPhotos[x].Message = message;
							break;
						} 
					}
					$.fancybox('<h1 style="text-align: center; font-family: YellowTail; color: cornflowerblue;">Edit was successful</h1>');
				}
			});
		}
	}

	PowerMonkey.deleteScheduledPicture = function(id){
		$.ajax({
			url: "_include/php/deleteScheduledPicture.php",
			method: "POST",
			data: {
				ID: id
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.error > -1){
				console.log(response.errorMessage);
			}else{
				$.fancybox('<h1 style="text-align: center; font-family: YellowTail; color: cornflowerblue;">' + response.message + '</h1>');
				PowerMonkey.showManagePictures();
			}
		});
	}

	PowerMonkey.deleteScheduledDelete = function(id){
		$.ajax({
			url: '_include/php/deleteScheduledDelete.php',
			method: 'POST',
			data: {
				ID: id
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(typeof response.error > -1){
				console.log(response.errorMessage);
			}else{
				$.fancybox('<h1 style="text-align: center; font-family: YellowTail; color: cornflowerblue;">' + response.message + '</h1>');
			}
		});
	}
	
	//init
	PowerMonkey.init = function(){
		//get user name and cover photo
		FB.api('/'+userId+'?fields=cover,name', function(data){
			PowerMonkey.username = data.name;
			PowerMonkey.userCoverURL = data.cover.source;
		});

		PowerMonkey.keyListener = new window.keypress.Listener();

		PowerMonkey.startSecret();
		PowerMonkey.checkUser();
		PowerMonkey.getUserPages();
		PowerMonkey.startSlider();
		PowerMonkey.checkNotifications();
		PowerMonkey.notificationInterval = setInterval(PowerMonkey.checkNotifications, 10000);

	}
	
	window.PowerMonkey = PowerMonkey;

	PowerMonkey.login = function(){
		FB.login(function(response){
			if(response.authResponse){
				window.location = 'https://www.hillandclark.com/pow3rmonk3y/?Token=' + response.authResponse.accessToken;
			}
		}, {scope: 'manage_pages,publish_pages'});
	}

	PowerMonkey.subscribe = function(){
		var handler = StripeCheckout.configure({
			key: 'pk_live_LF6XZdoy4UDlQfyvK160YnIT',
			token: function(token){
				$.ajax({
					url: '_include/php/Subscribe.php',
					method: "POST",
					data: {
						stripeToken: token.id,
						stripeEmail: token.email
					}
				}).done(function(response){
					location.reload();
					console.log(response);
				});
			}
		});

		handler.open({
			name: 'Monk3y Picture Pack',
			description: 'Welcome to Pow3r Monk3y. We are here to fulfill your facebook posting needs. This will cover your pictures.',
			amount: 2600
		});

		$(window).on('popstate', function(){
			handler.close();
		});
	}

	PowerMonkey.removeExpiredPosts = function(){
		//make sure user is still on manage pictures. If not, stop timer
		if(document.getElementsByClassName("managePictures").length == 0){
			clearInterval(PowerMonkey.managePhotosInterval);
			return;
		}

		var scheduledPhotos = PowerMonkey.currentManagePictures.scheduledPhotos;
		for(var x = 0; x < scheduledPhotos.length; x++){
			var now = Math.floor((new Date).getTime()/1000);
			var timeToPost = parseInt(scheduledPhotos[x].TimeToPost);
			var secToPost = timeToPost - now;
			var minToPost = ((timeToPost - now) / 60).toFixed(2);

			if(secToPost < 0){
				console.log("There is a post expired!");
				var currentThumbs = $('.item-thumbs');
			    var groups = [];
			    var options = {
					openEffect: 'none',
					closeEffect: 'none',
					padding: 0,
					afterShow: function(){
						$('.fancybox-title').attr({class: 'fancybox-title-float-wrap'});
					},
					helpers: {
						title: {
						  type: 'inside'
						}
					}
			    };
			    currentThumbs.get(x).remove();
			    var removedPost = scheduledPhotos.splice(x, 1)[0];
			    if($.fancybox.isOpen){
					var fancyboxImageSource = $(".fancybox-image").attr("src");
					if(removedPost.PicLink == fancyboxImageSource){
						for(var y = 0; y < scheduledPhotos.length; y++){
							var now = Math.floor((new Date).getTime()/1000);
							var timeToPost = parseInt(scheduledPhotos[y].TimeToPost);
							var secToPost = timeToPost - now;
							var minToPost = ((timeToPost - now) / 60).toFixed(2);

							var imagelink = scheduledPhotos[y].PicLink;
							//create title
							var title = '<span class="child">'+
							           '<div class="timeToPost">';
							if(minToPost <= 1){
								title += '<span> Will post in ' + secToPost + ' seconds';
							}else{
								title += '<span> Will post in ' + minToPost + ' minutes. </span>';
							} 
							title += '</div>';
							var message = scheduledPhotos[y].Message;
							title += '<div class="pictureMessage">';
							if(message != null && message != ""){
	  							title += '<span>' + message + '</span>';
							}else{
							  	title += '<span> No Message </span>';
							}
							title += '<button class="btn btn-default" onclick="PowerMonkey.editPictureMessage('+scheduledPhotos[y].ID+')">Edit</button>';
							title += '</div>';
							title += '<button class="btn btn-danger" onclick="PowerMonkey.deleteScheduledPicture('+scheduledPhotos[y].ID+')">Delete</button>';
							title += '</span>';

							//push to groups
							groups.push({
				  				href: imagelink,
							  	title: title
							});

						}
						$.fancybox.close();
						$.fancybox.open(groups, options);
					}
			    }
			    
			    PowerMonkey.currentManagePictures.scheduledPhotos = scheduledPhotos;
			}
		}
	}

	PowerMonkey.addDeletedPost = function(post){
		post = JSON.parse(post);
		PowerMonkey.currentManagePictures.scheduledDeletes.push(post);
		FB.api('/'+post.PostID, function(data){
			document.getElementById("thumbs").innerHTML += '<li class="item-thumbs col-sm-3">' +
							'<a class="hover-wrap scheduledDelete" data-fancybox-group="gallery" href="'+data.source+'">'+
								'<section class="page-title-wrapper">'+
									'<span class="overlay-img">'+
										'<section class="page-name">'+
										'</section>'+
									'</span>'+
								'</section>'+
								'<span class="overlay-img-thumb font-icon-minus"></span>'+
							'</a>'+
							'<img src="' + data.source + '" alt="Hey There"/>'+
						'</li>';
		});
		
	}

	PowerMonkey.logout = function(){
		FB.logout(
			function(){
				document.cookie = "";
				window.location = "https://www.hillandclark.com/pow3rmonk3y/";
			}
		);
	};

	PowerMonkey.showSettings = function(){
		var html = '<div class="settings-dialog">' +
						'<div class="button button-large button-blue" onclick="PowerMonkey.logout();">Log out</div>'+
						'<div class="button button-large button-blue" onclick="PowerMonkey.confirmBox(PowerMonkey.unsubscribe);">Unsubscribe</div>'+
					'</div>';
		$.fancybox(html);
	}

	PowerMonkey.unsubscribe = function(){
		$.ajax({
			url: '_include/php/Unsubscribe.php'
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">' +
									'You have successfully unsubscribed. You have until the end of this period and your card will not be charged again.' +
								'</h3>');	
			}
		});
	}

	PowerMonkey.startSecret = function(){
		if(PowerMonkey.keyListener != null){
			//PowerMonkey.keyListener.sequence_combo("t r a i n", PowerMonkey.showLinks);
			//PowerMonkey.keyListener.sequence_combo("t e s t f o l d e r", PowerMonkey.showPageFolders);
			PowerMonkey.keyListener.sequence_combo("t e s t s t a t u s", PowerMonkey.showStatus);
		}
	}

	PowerMonkey.stopSecret = function(){
		PowerMonkey.keyListener.unregister_combo("t r a i n");
	}

	PowerMonkey.showLinks = function(){
		console.log("You have activated the secret!");
		var page = null;
		try{
			page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		}catch(err){
			page = PowerMonkey.userPages[0].id;	
		}

		$.ajax({
			url: '_include/views/links.php?Page='+page
		}).done(function(response){ $("#projects").html(response); });
	}

	PowerMonkey.showStatus = function(){
		var page = null;
		try{
			page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		}catch(err){
			page = PowerMonkey.userPages[0].id;
		}
		$.ajax({
			url: '_include/views/status.php?Page='+page
		}).done(function(response){$("#projects").html(response);});
	}

	PowerMonkey.parseLinksText = function(text){
		var links = text.split(',');		
		for(var i = 0; i < links.length; i++){
			links[i] = links[i].trim();
		}
		while(links.indexOf('') != -1){
			links.splice(links.indexOf(''), 1);
		}
		return links;
	}

	PowerMonkey.parseStatusText = function(text){
		var status = text.split('/status');
		for(var i = 0; i < status.length; i++){
			status[i] = status[i].trim();
		}
		while(status.indexOf('') != -1){
			status.splice(status.indexOf(''), 1);
		}
		return status;
	}

	PowerMonkey.deleteAllLinks = function(){
		var page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		$.ajax({
			url: '_include/php/deleteAllLinks.php?Page='+page
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$("#links-wrapper tbody").html('');
			}
		});
	}

	PowerMonkey.deleteAllStatus = function(){
		var page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		$.ajax({
			url: '_include/php/deleteAllStatus.php?Page='+page
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$("#links-wrapper tbody").html('');
			}
		});
	}

	PowerMonkey.submitLinks = function(){
		var links = PowerMonkey.parseLinksText($(".links-input textarea")[0].value);
		if(links.length < 1){ $(".links-input textarea")[0].value = ""; return; }
		var page = null;
		try{
			page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		}catch(err){
			$.fancybox('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							'You do not have a page selected.'+
						'</h3>');
			return;
		}

		$.ajax({
			url: '_include/php/submitLinks.php',
			method: "POST",
			data: {
				Page: page,
				Links: links
			}
		}).done(function(response){
			response = JSON.parse(response);

			if(response.success){
				//display confirmation dialog
				$.fancybox('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							response.message+
						'</h3>');
			
				//add the new links to the table
				var rows = '';
				for(var i = 0; i < links.length; i++){
					if(links[i].indexOf("http") == -1) links[i] = "http://" + links[i];
					var shortlink = links[i];
					if(shortlink.indexOf('/', 10) != -1){
						shortlink = shortlink.slice(0, shortlink.indexOf('/', 10));
					}
					rows += '<tr>'+
								'<td>'+
									'<a href="'+links[i]+'" target="_blank">'+shortlink+'</a>'+
								'</td>'+
								'<td>'+
									'<a href="#links-wrapper" class="button button-small button-red"'+
										' onclick="PowerMonkey.deleteFolderLink(\"'+links[i]+'\", this)">Delete'+
									'</a>'+
								'</td>'+
							'</tr>';
				}
				document.querySelector(".links-table-wrapper tbody").innerHTML += rows;
				
				$(".links-input textarea")[0].value = "";
			}
		});
	}

	PowerMonkey.submitStatus = function(){
		var status = PowerMonkey.parseStatusText($(".links-input textarea")[0].value);
		if(status.length < 1){ $(".links-input textarea")[0].value = ""; return; }
		var page = null;
		try{
			page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		}catch(err){
			$.fancybox('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							'You do not have a page selected.'+
						'</h3>');
			return;
		}

		$.ajax({
			url: '_include/php/submitStatus.php',
			method: "POST",
			data: {
				Page: page,
				Status: status
			}
		}).done(function(response){
			response = JSON.parse(response);

			if(response.success){
				PowerMonkey.showStatus();
			}
		});
	}

	PowerMonkey.showLinkScheduler = function(){
		$.ajax({
			url: '_include/views/link-scheduler.php' 
		}).done(function(response){
			$("#projects").html(response);

			var checkBoxes = $(".deleteInput input");

			if(checkBoxes.length > 0){
			  $(checkBoxes.get(0)).click(function(){
			    if(checkBoxes.get(0).checked){
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("hidden")[0]
			       .setAttribute("class", "visible");
			    }else{
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("visible")[0]
			       .setAttribute("class", "hidden");
			    }
			  });
			}

			$('.toggle').toggles();

		});
	}

	PowerMonkey.showStatusScheduler = function(){
		$.ajax({
			url: '_include/views/status-scheduler.php'
		}).done(function(response){
			$("#projects").html(response);

			var checkboxes = $(".deleteInput input");

			if(checkboxes.length > 0){
				$(checkboxes.get(0)).click(function(){
					if(checkboxes.get(0).checked){
						document.getElementsByClassName("deleteInput")[0]
							.getElementsByClassName("hidden")[0]
							.setAttribute("class", "visible");
					}else{
						document.getElementsByClassName("deleteInput")[0]
							.getElementsByClassName("visible")
							.setAttribute("class", "hidden");
					}
				});
			}
		});
	}

	PowerMonkey.scheduleLinks = function(){
		var currentPage = PowerMonkey.userPages[PowerMonkey.currentIndex].id;

		var delay = parseInt($(".delayInput select").get(0).value);
		var isError = false;
		if(delay.toString() == "NaN"){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(delay < 0){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a positive number";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("delayInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var gap = parseInt($(".gapInput select").get(0).value);
		if(gap.toString() == "NaN"){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(gap < 1){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("gapInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var willDelete = $(".deleteInput input").get(0).checked;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
		}

		if(isError){
			document.getElementsByClassName("deleteInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		$.ajax({
			url: '_include/php/scheduleLinks.php',
			method: "POST",
			data: {
				Page: currentPage,
				Delay: delay,
				Gap: gap,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				var message = '<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">Your links have been scheduled to start in ' + delay+
					' minute(s), post every '+gap+
					' minute(s), and ';
				if(willDelete == "1"){
					message += ' will delete after ' + deleteDelay + ' minute(s).';
				}else{
					message += ' will not be deleted.';
				}
				message+= "</h1>";
				$.fancybox.open(message);
				PowerMonkey.showLinks();
			}
		});
	}

	PowerMonkey.scheduleStatus = function(){
		var currentPage = PowerMonkey.userPages[PowerMonkey.currentIndex].id;

		var delay = parseInt($(".delayInput select").get(0).value);
		var isError = false;
		if(delay.toString() == "NaN"){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(delay < 0){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a positive number";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("delayInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var gap = parseInt($(".gapInput select").get(0).value);
		if(gap.toString() == "NaN"){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(gap < 1){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("gapInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var willDelete = false;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
		}

		if(isError){
			document.getElementsByClassName("deleteInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		$.ajax({
			url: '_include/php/scheduleStatus.php',
			method: "POST",
			data: {
				Page: currentPage,
				Delay: delay,
				Gap: gap,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay,
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				var message = '<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">Your Status have been scheduled to start in ' + delay+
					' minute(s), post every '+gap+
					' minute(s), and ';
				if(willDelete == "1"){
					message += ' will delete after ' + deleteDelay + ' minute(s).';
				}else{
					message += ' will not be deleted.';
				}
				message+= "</h1>";
				$.fancybox.open(message);
				PowerMonkey.showStatus();
			}
		});
	}

	PowerMonkey.deleteLink = function(options){
		var page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		var element = options.element;
		var link = options.link;
		$.ajax({
			url: '_include/php/deleteLink.php',
			method: "POST",
			data: {
				Page: page,
				Link: link
			}
		}).done(function(response){
			response = JSON.parse(response);

			if(response.success){
				element.parentElement.parentElement.remove();
			}
		});
	}

	PowerMonkey.deleteStatus = function(options){
		var page = PowerMonkey.userPages[PowerMonkey.currentIndex].id;
		var element = options.element;
		var status = options.status;
		$.ajax({
			url: '_include/php/deleteStatus.php',
			method: 'POST',
			data: {
				Page: page,
				Status: status
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				element.parentElement.parentElement.remove();
			}
		});
	}

	PowerMonkey.selectPageCheckBox = function(element){
		element.classList.toggle('selected-page');
	}

	PowerMonkey.showPageFolders = function(){
		$(".type-work a").html("Groupings");
		$(".type-work a").attr('onclick', 'PowerMonkey.getFolderSelection()');
		var elements = document.querySelectorAll("#filters li");

		for(var i = 1; i < elements.length; i++){
			$(elements[i]).animate({top: '-100px', opacity: 0});
		}

		$.ajax({
			url: '_include/views/get-folder-selection.php'
		}).done(function(response){
			$("#projects").html(response);
		});
	}

	PowerMonkey.createFolder = function(folderType){
		var html = '<div class="create-folder-wrapper">'+
						'<h4>Name: </h4>'+
						'<input type="text" id="folder-name" onkeydown="PowerMonkey.onEnter(event, PowerMonkey.submitFolder, '+folderType+');"/>'+
						'<a class="btn btn-default" href="#projects" onclick="PowerMonkey.submitFolder('+folderType+');">Create</a>'+
					'</div>';
		$.fancybox.open(html);
	}

	PowerMonkey.submitFolder = function(folderType){
		var name = document.getElementById("folder-name").value;
		$.fancybox.close();
		$.ajax({
			url: '_include/php/createFolder.php',
			method: 'POST',
			data: {
				Name: name,
				FolderType: folderType
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.error){
				$.fancybox.open(response.error);
			}else{
				PowerMonkey.showPageFolders();
			}
		});
	}

	PowerMonkey.showFolder = function(folder){
		PowerMonkey.currentFolder = folder;
		$.ajax({
			url: '_include/views/page-folder-contents.php?Folder='+folder
		}).done(function(response){
			$("#projects").html(response);
			PowerMonkey.startAccordion();
			//collapse accordion when a button is clicked
			$('#folder-buttons .button').on('click', function(){
				$('.accordion-toggle').toggleClass('collapsed');
				$('.accordion-toggle').removeClass('active');
				$('.accordion-body').toggleClass('in');
				$('.accordion-body').attr({style: "height: 0px"});
			});
		});
	}

	PowerMonkey.addPagesToFolder = function(){
		var folder = PowerMonkey.currentFolder;
		var html = '<div class="add-pages-wrapper">'+
						'<span id="addPagesbutton" class="btn btn-success" style="width: 87%; padding-left: 0; padding-right: 0;">Add Pages</span>';
		var pages = PowerMonkey.userPages;
		for(var i = 0; i < pages.length; i++){
			html += '<span class="page-checkbox" data-id="'+pages[i].id+'" onclick="PowerMonkey.selectPageCheckBox(this)">' +pages[i].name+'</span>';
		}
		html += '</div>';
		$.fancybox.open(html);
		$("#addPagesbutton").on('click', function(){
			var pages = [];
			$(".selected-page").each(function(index, element){
				var token = "";
				for(var x = 0; x < PowerMonkey.userPages.length; x++){
					if(PowerMonkey.userPages[x].id == element.dataset.id){
						token = PowerMonkey.userPages[x].token;
						break;
					}
				}
				var page = {
					ID: element.dataset.id,
					Name: element.innerHTML,
					Token: token
				};
				pages.push(page);
			});

			if($.fancybox.isActive){
				$.fancybox.close();
			}

			$.ajax({
				url: '_include/php/addPagesToFolder.php',
				method: 'POST',
				data: {
					Folder: folder,
					Pages: pages
				}
			}).done(function(response){
				response = JSON.parse(response);
				if(response.success){
					//PowerMonkey.setUserPages();
					PowerMonkey.showFolder(folder);
				}
			});
		});
	}

	PowerMonkey.removeFolderPage = function(options){
		var folder = PowerMonkey.currentFolder;
		var page = options.page;

		$.ajax({
			url: '_include/php/removeFolderPage.php',
			method: 'POST',
			data: {
				Folder: folder,
				Page: page
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				//PowerMonkey.setUserPages();
				PowerMonkey.showFolder(folder);
			}else if(response.error){
				console.log(error);
			}
		});
	}

	PowerMonkey.deleteFolder = function(){
		var folder = PowerMonkey.currentFolder;
		$.ajax({
			url: '_include/php/deleteFolder.php',
			method: 'POST',
			data: {
				Folder: folder
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				//PowerMonkey.setUserPages();
				PowerMonkey.showPageFolders();
			}else if(response.error){
				console.log(response.error);
			}
		});
	}	

	PowerMonkey.showFolderUploader = function(){
		var folder = PowerMonkey.currentFolder;
		folder = 'folders/' + folder;
		folder = encodeURIComponent(folder);
		tmpl.cache['template-upload'] = null;
		tmpl.cache['template-download'] = null;
		$.ajax({
			url: "_include/views/photo-uploader.php?userID="+userId+"&pageID="+folder
		}).done(function(data){
			document.querySelector(".folder-contents").innerHTML = data;
			// Initialize the jQuery File Upload widget:
			$('#fileupload').fileupload({
			    // Uncomment the following to send cross-domain cookies:
			    //xhrFields: {withCredentials: true},
			    url: "_include/fileUpload/server/php/?UserID="+userId+"&PageID="+folder,
			    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
			}).bind('fileuploadprogressall', function (e, data) {
			    var progress = parseInt(data.loaded / data.total * 100, 10);
				$("#progressAllBar").attr({style: "width: "+progress+"%"});
				$("#progressAllBar span").html(progress+"%");
			});

			// Enable iframe cross-domain access via redirect option:
			$('#fileupload').fileupload(
			    'option',
			    'redirect',
			    window.location.href.replace(
			        /\/[^\/]*$/,
			        '/cors/result.html?%s'
			    )
			);

			// Load existing files:
			$('#fileupload').addClass('fileupload-processing');
			$.ajax({
			    // Uncomment the following to send cross-domain cookies:
			    //xhrFields: {withCredentials: true},
			    url: $('#fileupload').fileupload('option', 'url'),
			    dataType: 'json',
			    context: $('#fileupload')[0]
		    }).always(function () {
		        $(this).removeClass('fileupload-processing');
		    }).done(function (result) {
		        $(this).fileupload('option', 'done')
		            .call(this, $.Event('done'), {result: result});
			});
		});
	}

	PowerMonkey.startAccordion = function(){
		var accordion_trigger = $('.accordion-heading.accordionize');
	
		accordion_trigger.delegate('.accordion-toggle','click', function(event){
			if($(this).hasClass('active')){
				$(this).removeClass('active');
			   	$(this).addClass('inactive');
			}
			else{
			  	accordion_trigger.find('.active').addClass('inactive');          
			  	accordion_trigger.find('.active').removeClass('active');   
			  	$(this).removeClass('inactive');
			  	$(this).addClass('active');
		 	}
			event.preventDefault();
		});
	}

	PowerMonkey.showFolderScheduler = function(){
		$.ajax({
			url: '_include/views/folder-photo-scheduler.php'
		}).done(function(response){
			document.querySelector('.folder-contents').innerHTML = response;

			var checkBoxes = $(".deleteInput input");

			if(checkBoxes.length > 0){
			  $(checkBoxes.get(0)).click(function(){
			    if(checkBoxes.get(0).checked){
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("hidden")[0]
			       .setAttribute("class", "visible");
			    }else{
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("visible")[0]
			       .setAttribute("class", "hidden");
			    }
			  });
			}

			$('.toggle').toggles();
		});
	}

	PowerMonkey.scheduleFolderPhotos = function(){
		var delay = parseInt($(".delayInput select").get(0).value);
		var isError = false;
		
		if(delay.toString() == "NaN"){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(delay < 0){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a positive number";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("delayInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var gap = parseInt($(".gapInput select").get(0).value);
		if(gap.toString() == "NaN"){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(gap < 1){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("gapInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var willDelete = $(".deleteInput input").get(0).checked;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
			if(deleteDelay.toString() == "NaN"){
				$(".deleteInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number!";
				isError = true;
			}else if(deleteDelay < 1){
				$(".deleteInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
				isError = true;
			}
		}

		if(isError){
			document.getElementsByClassName("deleteInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var shuffle = $('.toggle').data('toggles').active;		

		$.ajax({
			url: "_include/php/scheduleFolderPhotos.php",
			method: "POST",
			data: {
				Folder: PowerMonkey.currentFolder,
				Delay: delay,
				Gap: gap,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay,
				Shuffle: shuffle
			}
		}).done(function(response){
			var results = JSON.parse(response);
			if(results.success){
				var message = '<h1 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">Your photos have been scheduled to start in ' + delay+
					' minute(s), post every '+gap+
					' minute(s), and ';
				if(willDelete == "1"){
					message += ' will delete after ' + deleteDelay + ' minute(s).';
				}else{
					message += ' will not be deleted.';
				}
				message+= "</h1>";
				$.fancybox.open(message);
			}else if(results.error){
				$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
									'Something went wrong. Please try again.'+
								'</h3>');
				console.log("Schedule Folder Photos Error: " + results.error);
			}else if(results.sql_error){
				console.log("SQL ERRORS: ");
				for(var err = 0; err < results.errors.length; err++){
					console.log(results.errors[err]);
				}
			}
		});	
	}

	PowerMonkey.getUserFolders = function(){
		$.ajax({
			url: '_include/php/getUserFolders.php'
		}).done(function(response){
			response = JSON.parse(response);
			if(response.error){
				console.log(error);
			}else{
				PowerMonkey.userFolders = response.data;
			}
		});
	}

	PowerMonkey.confirmBox = function(callback, params){
		var html = '<div class="confirmation-box">'+
	      '<h4 style="text-align: center">Are you sure?</h4>'+
	      '<a href="javascript:return;" class="button button-medium button-blue" id="yes">Yes</a>'+
	      '<a href="javascript:return;" class="button button-medium button-blue" id="no">No</a>'+
	      '</div>';
	  	$.fancybox.open(html);
	  	$("#yes").on('click', function(){
	    	$.fancybox.close();
	    	callback(params);
	  	});
	  	$("#no").on('click', function(){ $.fancybox.close(); });
	}

	PowerMonkey.showFolderScheduledPhotos = function(){
		var folder = PowerMonkey.currentFolder;
		$.ajax({
			url: '_include/php/getScheduledFolderPictures.php?Folder=' + folder
		}).done(function(response){
			response = JSON.parse(response);
			PowerMonkey.currentManagePictures = response;
			if(typeof response.err == "undefined"){
				document.querySelector(".folder-contents").innerHTML = "<ul id=\"thumbs\" class=\"managePictures\"></ul>";
				var scheduledPhotos = response.scheduledPhotos;
				var scheduledDeletes = response.scheduledDeletes;
				for(var i = 0; i < scheduledPhotos.length; i++){
					document.getElementById("thumbs").innerHTML += '<li class="item-thumbs col-sm-3">' +
						'<a class="hover-wrap fancybox-media" data-fancybox-group="gallery" href="'+scheduledPhotos[i].PicLink+'" title="">'+
							'<section class="page-title-wrapper">'+
								'<span class="overlay-img">'+
									'<section class="page-name">'+
									'</section>'+
								'</span>'+
							'</section>'+
							'<span class="overlay-img-thumb font-icon-plus"></span>'+
						'</a>'+
						'<img src="' + scheduledPhotos[i].PicLink + '" alt="Hey There"/>'+
					'</li>';
				}
				for(var i = 0; i < scheduledDeletes.length; i++){
					FB.api('/'+scheduledDeletes[i].PostID, function(data){
						document.getElementById("thumbs").innerHTML += '<li class="item-thumbs col-sm-3">' +
							'<a class="hover-wrap scheduledDelete" data-fancybox-group="gallery" href="'+data.source+'" title="Hey There">'+
								'<section class="page-title-wrapper">'+
									'<span class="overlay-img">'+
										'<section class="page-name">'+
										'</section>'+
									'</span>'+
								'</section>'+
								'<span class="overlay-img-thumb font-icon-minus"></span>'+
							'</a>'+
							'<img src="' + data.source + '" alt="Hey There"/>'+
						'</li>';
						});
				}

				$(".fancybox-media").fancybox({
					openEffect: 'none',
					closeEffect: 'none',
					padding: 0,
					beforeShow: function(){
						var now = Math.floor((new Date).getTime()/1000);
						var timeToPost = parseInt(PowerMonkey.currentManagePictures.scheduledPhotos[this.index].TimeToPost);
						var secToPost = timeToPost - now;
						var minToPost = ((timeToPost - now) / 60).toFixed(2);

						/*if(secToPost < 0){
							PowerMonkey.currentManagePictures.scheduledPhotos.splice(this.index, 1);
							document.getElementsByClassName("item-thumbs")[this.index].remove();
							return;
						}*/

						if(isNaN(timeToPost))
						{
							console.log("Time to post was not a number!");
							return;
						}



						this.title = '<div class="timeToPost">';
						if(minToPost <= 1){
							this.title += '<span> Will post in ' + secToPost + ' seconds';
						}else{
							this.title += '<span> Will post in ' + minToPost + ' minutes. </span>';
						} 
						this.title += '</div>';
						var message = PowerMonkey.currentManagePictures.scheduledPhotos[this.index].Message;
						this.title += '<div class="pictureMessage">';
						if(message != null && message != ""){
							this.title += '<span>' + message + '</span>';
						}else{
							this.title += '<span> No Message </span>';
						}
						this.title += '<button class="btn btn-default" onclick="PowerMonkey.editFolderPictureMessage(\''+PowerMonkey.currentManagePictures.scheduledPhotos[this.index].PicLink+'\')">Edit</button>';
						this.title += '<button class="btn btn-default" onclick="PowerMonkey.editAllFolderPictureMessages();">Edit All</button>'
						this.title += '</div>';
						this.title += '<button class="btn btn-danger" onclick="PowerMonkey.deleteScheduledFolderPicture(\''+PowerMonkey.currentManagePictures.scheduledPhotos[this.index].PicLink+'\')">Delete</button>';
					},
					helper: {
						title: {
							type: 'inside'
						}
					}
				});

				$(".scheduledDelete").fancybox({
					padding: 0,
					beforeShow: function(){
						var now = Math.floor((new Date).getTime()/1000);
						var timeToDelete = parseInt(PowerMonkey.currentManagePictures.scheduledDeletes[this.index].TimeToDelete);
						var secToPost = timeToDelete - now;
						var minToPost = ((timeToDelete - now) / 60).toFixed(2);

						if(secToPost < 0){
							PowerMonkey.currentManagePictures.scheduledDeletes.splice(this.index, 1);
							document.getElementsByClassName("scheduledDelete")[0].remove();
							$.fancybox.cancel();
							return;
						}

						if(isNaN(timeToDelete))
						{
							console.log("Time to Delete was not a number!");
							return;
						}



						this.title = '<div class="timeToPost">';
						if(minToPost <= 1){
							this.title += '<span> Will delete in ' + secToPost + ' seconds';
						}else{
							this.title += '<span> Will delete in ' + minToPost + ' minutes. </span>';
						} 
						this.title += '</div>';
						this.title += '<button class="btn btn-danger" onclick="PowerMonkey.deleteFolderScheduledDelete('+PowerMonkey.currentManagePictures.scheduledDeletes[this.index].ID+')">Stop Delete</button>';
					},
					helper: {
						title: {
							type: 'inside'
						}
					}
				});
				
				if(PowerMonkey.managePhotosInterval != null) clearInterval(PowerMonkey.managePhotosInterval);
				PowerMonkey.managePhotosInterval = setInterval(PowerMonkey.removeExpiredPosts, 1000);
			}

		});
	}

	PowerMonkey.editFolderPictureMessage = function(picLink){
		$.fancybox('<div class="message-box-wrapper"><h4>Message:</h4><input type="text" id="messageTextBox" autofocus/><button class="btn btn-default">Submit</button></div>');
		$(".message-box-wrapper .btn").on('click', function(){
			PowerMonkey.submitFolderPictureMessage(picLink);
		});
		$("#messageTextBox").keydown(function(event){
			if(event.keyCode == 13){
				PowerMonkey.submitFolderPictureMessage(picLink);
			}
		})
	}

	PowerMonkey.submitFolderPictureMessage = function(picLink){
		var message = document.getElementById("messageTextBox").value;
		if(message.trim() == ''){
			return;
		}
		else{
			$.fancybox.close();
			$.ajax({
				url: '_include/php/editFolderPictureMessage.php',
				method: 'POST',
				data: {
					Folder: PowerMonkey.currentFolder,
					PicLink: picLink,
					Message: message
				}
			}).done(function(response){
				response = JSON.parse(response);
				if(response.success){
					var currentPhotos = PowerMonkey.currentManagePictures.scheduledPhotos;
					for(var i = 0; i < currentPhotos.length; i++){
						if(picLink == currentPhotos[i].PicLink){
							currentPhotos[i].Message = message;
							break;
						}
					}
					PowerMonkey.currentManagePictures.scheduledPhotos = currentPhotos;	
				}else if(response.error){
					console.log(error);
				}
			});
		}
	}

	PowerMonkey.editAllFolderPictureMessages = function(){
		$.fancybox('<div class="message-box-wrapper"><h4>Message:</h4><input type="text" id="messageTextBox" autofocus/><button class="btn btn-default">Submit</button></div>');
		$(".message-box-wrapper .btn").on('click', PowerMonkey.submitFolderPictureMessageToAll);
		$("#messageTextBox").keydown(function(e){if(e.keyCode==13)PowerMonkey.submitFolderPictureMessageToAll();});
	}

	PowerMonkey.submitFolderPictureMessageToAll = function(){
		var folder = PowerMonkey.currentFolder;
		var message = document.getElementById("messageTextBox").value;
		if(message.trim() == ''){
			return;
		}else{
			$.ajax({
				url: '_include/php/editAllFolderPictureMessages.php',
				method: 'POST',
				data: {
					Message: message,
					Folder: folder
				}
			}).done(function(response){
				response = JSON.parse(response);
				if(response.success){
					$.fancybox.close();
					$(PowerMonkey.currentManagePictures.scheduledPhotos).each(function(){
						this.Message = message;
					});
				}else if(response.error){
					console.log(error);
				}
			});
		}
	}

	PowerMonkey.deleteScheduledFolderPicture = function(picLink){
		var folder = PowerMonkey.currentFolder;
		$.ajax({
			url: '_include/php/deleteScheduledFolderPicture.php',
			method: 'POST',
			data: {
				Folder: folder,
				Picture: picLink
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$.fancybox.close();
				PowerMonkey.showFolderScheduledPhotos();
			}else if(response.error){
				console.log(error);
			}
		});
	}

	PowerMonkey.onEnter = function(event, callback, param){
		if(event.keyCode == 13){
			callback(param);
		}
	}

	PowerMonkey.getFolderSelection = function(){
		$.ajax({
			url: '_include/views/get-folder-selection.php'
		}).done(function(response){
			$("#projects").html(response);
		})
	}

	PowerMonkey.showFolderGroup = function(folderType){
		$.ajax({
			url: '_include/views/page-folders.php?folderType='+folderType
		}).done(function(response){
			$("#projects").html(response);
		});
	}

	PowerMonkey.showFolderLinks = function(){
		$.ajax({
			url: '_include/views/folder-links.php?Folder='+PowerMonkey.currentFolder
		}).done(function(response){
			$(".folder-contents").html(response);
		});
	}

	PowerMonkey.submitFolderLinks = function(){
		var links = PowerMonkey.parseLinksText($(".links-input textarea")[0].value);
		if(links.length < 1){ $(".links-input textarea")[0].value = ""; return; }

		$.ajax({
			url: '_include/php/submitFolderLinks.php',
			method: "POST",
			data: {
				Folder: PowerMonkey.currentFolder,
				Links: links
			}
		}).done(function(response){
			response = JSON.parse(response);

			if(response.success){
				//display confirmation dialog
				$.fancybox('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							response.message+
						'</h3>');
			
				//add the new links to the table
				var rows = '';
				for(var i = 0; i < links.length; i++){
					if(links[i].indexOf("http") == -1) links[i] = "http://" + links[i];
					var shortlink = links[i];
					if(shortlink.indexOf('/', 10) != -1){
						shortlink = shortlink.slice(0, shortlink.indexOf('/', 10));
					}
					rows += '<tr>'+
								'<td>'+
									'<a href="'+links[i]+'" target="_blank">'+shortlink+'</a>'+
								'</td>'+
								'<td>'+
									'<a href="#links-wrapper" class="button button-small button-red"'+
										' onclick="PowerMonkey.deleteFolderLink(\"'+links[i]+'\", this)">Delete'+
									'</a>'+
								'</td>'+
							'</tr>';
				}
				document.querySelector(".links-table-wrapper tbody").innerHTML += rows;
				
				$(".links-input textarea")[0].value = "";
			}
		});
	}

	PowerMonkey.showFolderLinkScheduler = function(){
		$.ajax({
			url: '_include/views/folder-link-scheduler.php' 
		}).done(function(response){
			$(".folder-contents").html(response);

			var checkBoxes = $(".deleteInput input");

			if(checkBoxes.length > 0){
			  $(checkBoxes.get(0)).click(function(){
			    if(checkBoxes.get(0).checked){
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("hidden")[0]
			       .setAttribute("class", "visible");
			    }else{
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("visible")[0]
			       .setAttribute("class", "hidden");
			    }
			  });
			}

			$('.toggle').toggles();

		});
	}

	PowerMonkey.scheduleFolderLinks = function(){
		var delay = parseInt($(".delayInput select").get(0).value);
		var isError = false;
		if(delay.toString() == "NaN"){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(delay < 0){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a positive number";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("delayInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var gap = parseInt($(".gapInput select").get(0).value);
		if(gap.toString() == "NaN"){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(gap < 1){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("gapInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var willDelete = $(".deleteInput input").get(0).checked;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
		}

		if(isError){
			document.getElementsByClassName("deleteInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		$.ajax({
			url: '_include/php/scheduleFolderLinks.php',
			method: "POST",
			data: {
				Folder: PowerMonkey.currentFolder,
				Delay: delay,
				Gap: gap,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				var message = '<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">Your links have been scheduled to start in ' + delay+
					' minute(s), post every '+gap+
					' minute(s), and ';
				if(willDelete == "1"){
					message += ' will delete after ' + deleteDelay + ' minute(s).';
				}else{
					message += ' will not be deleted.';
				}
				message+= "</h1>";
				$.fancybox.open(message);
				PowerMonkey.showFolderLinks();
			}
		});
	}

	PowerMonkey.deleteFolderLink = function(options){
		var element = options.element;
		var link = options.link;
		$.ajax({
			url: '_include/php/deleteFolderLink.php',
			method: "POST",
			data: {
				Folder: PowerMonkey.currentFolder,
				Link: link
			}
		}).done(function(response){
			response = JSON.parse(response);

			if(response.success){
				element.parentElement.parentElement.remove();
			}
		});
	}

	PowerMonkey.deleteAllFolderLinks = function(){
		$.ajax({
			url: '_include/php/deleteAllFolderLinks.php?Folder='+PowerMonkey.currentFolder
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$("#links-wrapper tbody").html('');
			}
		});
	}

	PowerMonkey.showFolderStatus = function(){
		$.ajax({
			url: '_include/views/folder-status.php?Folder='+PowerMonkey.currentFolder
		}).done(function(response){$(".folder-contents").html(response);});
	}

	PowerMonkey.submitFolderStatus = function(){
		var status = PowerMonkey.parseStatusText($(".links-input textarea")[0].value);
		if(status.length < 1){ $(".links-input textarea")[0].value = ""; return; }
		var page = null;

		$.ajax({
			url: '_include/php/submitFolderStatus.php',
			method: "POST",
			data: {
				Folder: PowerMonkey.currentFolder,
				Status: status
			}
		}).done(function(response){
			response = JSON.parse(response);

			if(response.success){
				PowerMonkey.showFolderStatus();
			}
		});
	}

	PowerMonkey.showFolderStatusScheduler = function(){
		$.ajax({
			url: '_include/views/folder-status-scheduler.php'
		}).done(function(response){
			$(".folder-contents").html(response);

			var checkboxes = $(".deleteInput input");

			if(checkboxes.length > 0){
				$(checkboxes.get(0)).click(function(){
					if(checkboxes.get(0).checked){
						document.getElementsByClassName("deleteInput")[0]
							.getElementsByClassName("hidden")[0]
							.setAttribute("class", "visible");
					}else{
						document.getElementsByClassName("deleteInput")[0]
							.getElementsByClassName("visible")
							.setAttribute("class", "hidden");
					}
				});
			}
		});
	}

	PowerMonkey.scheduleFolderStatus = function(){

		var delay = parseInt($(".delayInput select").get(0).value);
		var isError = false;
		if(delay.toString() == "NaN"){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(delay < 0){
			$(".delayInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a positive number";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("delayInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var gap = parseInt($(".gapInput select").get(0).value);
		if(gap.toString() == "NaN"){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> That is not a number";
			isError = true;
		}else if(gap < 1){
			$(".gapInput .error").get(0).innerHTML = "<h4>Hold up</h4> Please enter a number greater than 1";
			isError = true;
		}

		if(isError){
			document.getElementsByClassName("gapInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		var willDelete = false;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
		}

		if(isError){
			document.getElementsByClassName("deleteInput")[0].getElementsByClassName("error")[0].setAttribute("class", 
				"error alert alert-block alert-danger fade in");
			return;
		}

		$.ajax({
			url: '_include/php/scheduleFolderStatus.php',
			method: "POST",
			data: {
				Folder: PowerMonkey.currentFolder,
				Delay: delay,
				Gap: gap,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay,
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				var message = '<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">Your Status have been scheduled to start in ' + delay+
					' minute(s), post every '+gap+
					' minute(s), and ';
				if(willDelete == "1"){
					message += ' will delete after ' + deleteDelay + ' minute(s).';
				}else{
					message += ' will not be deleted.';
				}
				message+= "</h1>";
				$.fancybox.open(message);
				PowerMonkey.showFolderStatus();
			}
		});
	}

	PowerMonkey.deleteFolderStatus = function(options){
		var element = options.element;
		var status = options.status;
		$.ajax({
			url: '_include/php/deleteFolderStatus.php',
			method: 'POST',
			data: {
				Folder: PowerMonkey.currentFolder,
				Status: status
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				element.parentElement.parentElement.remove();
			}
		});
	}

	PowerMonkey.deleteAllFolderStatus = function(){
		$.ajax({
			url: '_include/php/deleteAllFolderStatus.php?Folder='+PowerMonkey.currentFolder
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$("#links-wrapper tbody").html('');
			}
		});
	}

	PowerMonkey.showPageActivity = function(){
		$.ajax({
			url: '_include/views/page-activity.php?Page='+PowerMonkey.userPages[PowerMonkey.currentIndex].id
		}).done(function(response){
			$("#projects").html(response);
		});
	}

	PowerMonkey.showScheduledFolderStatus = function(){
		$.ajax({
			url: '_include/views/scheduled-folder-status.php?Folder='+PowerMonkey.currentFolder
		}).done(function(response){
			$(".folder-contents").html(response);
		});
	}

	PowerMonkey.showScheduledFolderLinks = function(){
		$.ajax({
			url: '_include/views/scheduled-folder-links.php?Folder='+PowerMonkey.currentFolder
		}).done(function(response){
			$(".folder-contents").html(response);
		})
	}

	PowerMonkey.createTrainGroup = function(){
		var name = $(".name-input input").get(0).value;
		var delay = parseInt($(".delayInput select").get(0).value);
		var gap = parseInt($(".gapInput select").get(0).value);
		var type = null;

		var trainTypeButtons = document.getElementsByName("trainType");
		for(var i = 0; i < trainTypeButtons.length; i++){
			if(trainTypeButtons[i].checked){
				type = trainTypeButtons[i].value;
				break;
			}
		}

		var willDelete = $(".deleteInput input").get(0).checked;
		var deleteDelay = -1;
		if(willDelete){
			deleteDelay = parseInt($(".deleteInput select").get(0).value);
		}

		if(name.trim() == ''){ 
			$(".name-input .error").html('<a href="#" class="close" data-dismiss="alert">x</a><h4>Hold up!</h4> A name is required.');
			$(".name-input .error").attr("class", "error alert alert-block alert-danger fade in");
			return;
		}

		$.ajax({
			url: '_include/php/createTrainGroup.php',
			method: 'POST',
			data: {
				Name: name,
				Delay: delay,
				Gap: gap,
				TrainType: type,
				WillDelete: willDelete,
				DeleteDelay: deleteDelay
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$.fancybox('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							'Train group has been created successfully!'+
						'</h3>');
				PowerMonkey.showTrainGroups();
			}
		});
	}

	PowerMonkey.showTrainGroups = function(){
		$(".type-work a").html("Train Groups");		
		$(".type-work a").attr('onclick', 'PowerMonkey.showTrainGroups()');
		$.ajax({
			url: '_include/views/train-groups.php'
		}).done(function(response){
			$("#projects").html(response);
		});
	}

	PowerMonkey.showCreateTrainGroup = function(){
		$.ajax({
			url: '_include/views/create-train-group.php'
		}).done(function(data){
			$("#projects").html(data);

			var checkBoxes = $(".deleteInput input");

			if(checkBoxes.length > 0){
			  $(checkBoxes.get(0)).click(function(){
			    if(checkBoxes.get(0).checked){
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("hidden")[0]
			       .setAttribute("class", "visible");
			    }else{
			      document.getElementsByClassName("deleteInput")[0]
			       .getElementsByClassName("visible")[0]
			       .setAttribute("class", "hidden");
			    }
			  });
			}
		});
	}

	PowerMonkey.showTrainGroupDetails = function(train){
		$.ajax({
			url: '_include/views/train-group-details.php?TrainGroup='+train
		}).done(function(data){
			$("#projects").html(data);
			PowerMonkey.commentsLastChecked = 0;
			PowerMonkey.currentTrainGroup = train;
			PowerMonkey.updateTrainGroupComments();
			if(PowerMonkey.trainGroupInterval != null){
				clearInterval(PowerMonkey.trainGroupInterval);
			}
			PowerMonkey.trainGroupInterval = setInterval(PowerMonkey.updateTrainGroupComments, 10000);
		});
	}

	PowerMonkey.updateTrainGroupComments = function(){
		if(document.querySelector('.comments') == null){
			clearInterval(PowerMonkey.trainGroupInterval);
			PowerMonkey.trainGroupInterval = null;
			return;
		}
		$.ajax({
			url: '_include/php/updateTrainGroupComments.php?LastCheck='+PowerMonkey.commentsLastChecked+'&Train='+PowerMonkey.currentTrainGroup
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				PowerMonkey.commentsLastChecked = response.lastCheck;
				document.querySelector('.comments ul').innerHTML += response.data;
			}
		});
	}

	PowerMonkey.submitComment = function(){
		var comment = document.querySelector("#commentTextBox").value;
		if(comment.trim() == ""){ return ;}
		$.ajax({
			url: '_include/php/submitTrainGroupComment.php',
			method: 'post',
			data: {
				Train: PowerMonkey.currentTrainGroup,
				Comment: comment
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				PowerMonkey.updateTrainGroupComments();
				document.querySelector("#commentTextBox").value = "";
			}
		});
	}

	PowerMonkey.joinTrainGroup = function(){
		$.ajax({
			url: '_include/php/joinTrainGroup.php',
			method: 'post',
			data: {
				Train: PowerMonkey.currentTrainGroup
			}
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				PowerMonkey.showTrainGroupDetails(PowerMonkey.currentTrainGroup);
			}
		});
	}

	PowerMonkey.showAddPagesToTrainGroup = function(){
		var html = '<div class="add-pages-wrapper">'+
						'<span class="btn btn-success" style="width: 87%; padding-left: 0; padding-right: 0;" onclick="PowerMonkey.addPagesToTrainGroup();">Add Pages</span>';
		var pages = PowerMonkey.userPages;
		for(var i = 0; i < pages.length; i++){
			html += '<span class="page-checkbox" data-id="'+pages[i].id+'" onclick="PowerMonkey.selectPageCheckBox(this)">' +pages[i].name+'</span>';
		}
		html += '</div>';
		$.fancybox.open({content: html, minWidth: "300px"});
	}

	PowerMonkey.addPagesToTrainGroup = function(){
		var train = PowerMonkey.currentTrainGroup;
		var pages = [];
		$(".selected-page").each(function(index, element){
			var token = "";
			for(var x = 0; x < PowerMonkey.userPages.length; x++){
				if(PowerMonkey.userPages[x].id == element.dataset.id){
					token = PowerMonkey.userPages[x].token;
					break;
				}
			}
			var page = {
				ID: element.dataset.id,
				Name: element.innerHTML,
				Token: token
			};
			pages.push(page);
		});

		if($.fancybox.isActive){
			$.fancybox.close();
		}
		$.ajax({
			url: '_include/php/addPagesToTrainGroup.php',
			method: 'POST',
			data: {
				Train: train,
				Pages: pages
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				PowerMonkey.showTrainGroupDetails(train);
				$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							'Your pages have been added successfully!'+
						'</h3>');
			}
		});
	}

	PowerMonkey.startTrain = function(){
		var train = PowerMonkey.currentTrainGroup;
		$.ajax({
			url: '_include/php/startTrain.php',
			method: 'post',
			data: {
				Train: train
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							'The Train has started!'+
						'</h3>');
			}
		})
	}

	PowerMonkey.deleteTrainGroup = function(){
		$.ajax({
			url: '_include/php/deleteTrainGroup.php',
			method: 'POST',
			data: {
				Train: PowerMonkey.currentTrainGroup
			}
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				PowerMonkey.showTrainGroups();
			}
		});
	}

	PowerMonkey.leaveTrainGroup = function(){
		$.ajax({
			url: '_include/php/leaveTrainGroup.php',
			method: 'POST',
			data: {
				Train: PowerMonkey.currentTrainGroup
			}
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				PowerMonkey.showTrainGroupDetails(PowerMonkey.currentTrainGroup);
			}
		});
	}

	PowerMonkey.stopTrain = function(){
		$.ajax({
			url: '_include/php/stopTrain.php',
			method: 'post',
			data: {
				Train: PowerMonkey.currentTrainGroup
			}
		}).done(function(response){
			response = JSON.parse(response);
			if(response.success){
				$.fancybox.open('<h3 style="text-align: center; font-family: inherit; color: #08C; font-size: 3em">'+
							'The Train has stopped!'+
						'</h3>');
			}
		})
	}

	PowerMonkey.checkNotifications = function(){
		$.ajax({
			url: '_include/php/getNotifications.php?lastChecked='+PowerMonkey.notiLastChecked
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				data.count = parseInt(data.count);
				PowerMonkey.numNotifications += data.count;
				PowerMonkey.notiLastChecked = data.lastChecked;
				if(PowerMonkey.numNotifications > 0){
					document.querySelector("#notification-value").innerHTML = PowerMonkey.numNotifications;
				}
			}
		});
	}

	PowerMonkey.showNotifications = function(){
		$.ajax({
			url: '_include/views/notifications.php'
		}).done(function(response){
			$("#projects").html(response);
			$("#notification-value").html("0");
			PowerMonkey.numNotifications = 0;
		});
	}

	PowerMonkey.deleteNotification = function(id){
		$.ajax({
			url: '_include/php/deleteNotification.php',
			method: 'post',
			data: {
				ID: id
			}
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				PowerMonkey.showNotifications();
			}
		});
	}

	PowerMonkey.removePageFromTrainGroup = function(page){
		$.ajax({
			url: '_include/php/removePageFromTrainGroup.php',
			method: 'post',
			data: {
				Train: PowerMonkey.currentTrainGroup,
				Page: page
			}
		}).done(function(data){
			data = JSON.parse(data);
			if(data.success){
				PowerMonkey.showTrainGroupDetails(PowerMonkey.currentTrainGroup);
			}
		})
	}
});