<div class="scheduler">
	<div class="scheduler-form">
		<div class="delayInput">
			<span class="label label-default">Start in: </span>
			<select class="btn btn-primary">
				<option value="1">1 minute</option>
				<option value="2">2 minutes</option>
				<option value="3">3 minutes</option>
				<option value="4">4 minutes</option>
				<option value="5">5 minutes</option>
			</select>
			<div class="error"></div>
		</div>
		<div class="gapInput">
			<span class="label label-default">Post every: </span>
			<select class="btn btn-primary">
				<option value="15">15 minutes</option>
				<option value="30">30 minutes</option>
				<option value="45">45 minutes</option>
				<option value="60">1 hour</option>
				<option value="120">2 hours</option>
				<option value="240">4 hours</option>
				<option value="480">8 hours</option>
			</select>
			<div class="error"></div>
		</div>
		<div class="deleteInput">
			<span class="label label-default">Delete these posts later?</span>
			<input type="checkbox" name="deleteCheckBox">
			<div class="hidden">
				<span class="label label-default">Delete in: </span>
				<select class="btn btn-primary">
					<option value="15">15 minutes</option>
					<option value="30">30 minutes</option>
					<option value="45">45 minutes</option>
					<option value="60">1 hour</option>
					<option value="120">2 hours</option>
					<option value="240">4 hours</option>
					<option value="480">8 hours</option>
					<option value="720">12 hours</option>
					<option value="1440">24 hours</option>
				</select>
				<div class="error"></div>
			</div>
		</div>
		<div class="submitInput">
			<button class="btn btn-info" onclick="PowerMonkey.scheduleLinks()">Schedule</button>
		</div>
	</div>
</div>