<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$connection = ConnectToDB::connect();
$sql = 'SELECT * FROM Notifications WHERE UserID="'.$user->getUserID().'"';
$results = $connection->query($sql);
$sql = 'UPDATE Notifications SET Seen=1 WHERE UserID="'.$user->getUserID().'"';
$connection->query($sql);
?>

<div class="row">
	<div class="col-sm-12">
		<table class="notifications-table">
			<thead>
				<tr>
					<td colspan="2">
						<h3>Notifications</h3>
					</td>
				</tr>
			</thead>
			<tbody>
				<?if($results->num_rows > 0){
					while($row = $results->fetch_assoc()){
				?>
					<tr>
						<td>
							<?echo $row['Message'];?>
						</td>
						<td>
							<a href="javascript:void(0)" class="button button-small button-blue" onclick="PowerMonkey.deleteNotification(<?php echo $row["ID"];?>)">Dismiss</a> 
						</td>
					</tr>
				<?	}//end while
				}else{
				?>
					<tr>
						<td class="trains-error">
							No Notifications
						</td>
					</tr>
				<?
				}
				?>
			</tbody>
		</table>
	</div>
</div>