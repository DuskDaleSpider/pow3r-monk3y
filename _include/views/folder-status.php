<?php
require_once ('../php/PowerMonkey/User.php');
require_once ('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION["User"];
$folder = $_GET["Folder"];
$status = array();

if($user != null && $user != ""){
	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM FolderStatus WHERE FolderID='.$folder;
	$status = $connection->query($sql);
	$connection->close();
}else{
	$response = array(
		"error" => "User is not logged in!"
	);
	die(json_encode($response));
}
?>
<div id="links-wrapper">
	<section class="links-input">
		<div class="link-description">
			Type in the status you want to add that are seperated by /status.
		</div>
		<textarea class="links-textbox" />
		<a href="#links-wrapper" class="button button-small button-blue"
		   onclick="PowerMonkey.submitFolderStatus()">Add Status</a>
	</section>
	<section class="links-table-wrapper">
		<table>
			<thead>
				<tr>
					<td>Your Status:</td>
					<td>
						<a href="#links-wrapper" class="button button-small button-red" onclick="PowerMonkey.confirmBox(PowerMonkey.deleteAllFolderStatus)">Delete All</a>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php
					while($message = $status->fetch_assoc()){
						$statusID = $message['ID'];
						?>
						<tr>
							<td>
								<?php echo $message['Status']; ?>
							</td>
							<td>
								<a href="#links-wrapper" class="button button-small button-red"
								   onclick='PowerMonkey.confirmBox(PowerMonkey.deleteFolderStatus, {status: "<?php echo $statusID;?>", element: this});'>
								   	Delete
								</a>
							</td>
						</tr>
						<?
					}
				?>
			</tbody>
		</table>
	</section>
</div>	