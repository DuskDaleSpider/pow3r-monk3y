<?php

    require_once ( '_include/php/User.php' );
    require_once ( '_include/php/Facebook/Entities/AccessToken.php' );
    require_once ( '_include/php/Facebook/FacebookSession.php' );
    require_once ( '_include/php/Facebook/FacebookSDKException.php');
    require_once ( '_include/php/Facebook/FacebookRequestException.php');
    require_once ( '_include/php/Facebook/FacebookOtherException.php');
    require_once ( '_include/php/Facebook/FacebookRequestException.php');
    require_once ( '_include/php/Facebook/FacebookAuthorizationException.php');
    require_once ( '_include/php/Facebook/HttpClients/FacebookHttpable.php' );
    require_once ( '_include/php/Facebook/HttpClients/FacebookCurl.php' );
    require_once ( '_include/php/Facebook/HttpClients/FacebookCurlHttpClient.php' );
    require_once ( '_include/php/Facebook/FacebookRequest.php' );
    require_once ( '_include/php/Facebook/FacebookResponse.php' );
    require_once ( '_include/php/Facebook/GraphObject.php' );
    require_once ( '_include/php/Facebook/GraphSessionInfo.php' );
    require_once ( '_include/php/Facebook/FacebookServerException.php');
    require_once ( '_include/php/Facebook/FacebookThrottleException.php');
    require_once ( '_include/php/Facebook/FacebookRedirectLoginHelper.php');
    require_once ( '_include/php/stripe-php/init.php');

    use Facebook\Entities\FacebookAccessToken;
    use Facebook\FacebookSession;
    use Facebook\FacebookSDKException;
    use Facebook\FacebookOtherException;
    use Facebook\HttpClients\FacebookHttpable;
    use Facebook\HttpClients\FacebookCurl;
    use Facebook\HttpClients\FacebookCurlHttpClient;
    use Facebook\FacebookRequestException;
    use Facebook\FacebookAuthorizationException;
    use Facebook\FacebookRequest;
    use Facebook\FacebookResponse;
    use Facebook\GraphObject;
    use Facebook\GraphSessionInfo;
    use Facebook\FacebookServerException;
    use Facebook\FacebookThrottleException;
    use Facebook\FacebookRedirectLoginHelper;

    $coverPhoto = (new FacebookRequest($session, 'GET', '/me?fields=cover'))->execute()->getGraphObject()->asArray();
    $user = (new FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject()->asArray();
?>
<script>
    var slides = [
        {image : "<?php echo $coverPhoto["cover"]->source;?>", title : '', thumb : '', url : ''}
    ];
</script>

<!-- This section is for Splash Screen -->
<div class="ole">
<section id="jSplash">
	<div id="circle"></div>
</section>
</div>
<!-- End of Splash Screen -->

<!-- Homepage Slider -->
<div id="home-slider">	
    <div class="overlay"></div>

    <div class="slider-text">
    	<div id="slidecaption"></div>
    </div>   
	
</div>
<!-- End Homepage Slider -->

<!-- Header -->
<header>
    <div class="sticky-nav">
    	<a id="mobile-nav" class="menu-nav" href="#menu-nav"></a>
        
        <div id="logo">
        	<a  title="Pow3r Monk3y Facebook Group" href="https://www.facebook.com/groups/1420914664875812/" target="_blank">Pow3r Monk3y</a>
        </div>
        
        <nav id="menu">
        	<ul id="menu-nav">
                <li class="notifications" onclick="PowerMonkey.showNotifications()"><span id="notification-value">0</span></li>
            	<li class="current"><a href="#" onclick="PowerMonkey.showPageThumbs();">Pages</a></li>
                <li><a href="#projects" onclick="PowerMonkey.showPageFolders()">Groupings</a></li>
                <li><a href="#projects" onclick="PowerMonkey.showTrainGroups()">Trains</a></li>
                <li onclick="PowerMonkey.showSettings();"><a href="#">Settings</a></li>
            </ul>
        </nav>
        
    </div>
</header>
<!-- End Header -->

<!-- Our Work Section -->
<div id="work" class="page">
	<div class="container">
    	<!-- Title Page -->
        <div class="row">
            <div class="col-sm-12">
                <div class="title-page">
                    <h2 class="title"><?php echo $user["name"]; ?></h2>
                </div>
            </div>
        </div>
        <!-- End Title Page -->
        
        <!-- Portfolio Projects -->
        <div class="row">
        	<div class="col-sm-3">
            	<!-- Filter -->
                <nav id="options" class="work-nav">
                    <ul id="filters" class="option-set" data-option-key="filter">
                    	<li class="type-work" style="z-index: 200;"><a class="selected" onclick="PowerMonkey.showPageThumbs(); return false;">Choose Page</a></li>
                        <li><a href="#filter" data-option-value="*" class="">Upload Pictures</a></li>
                        <li><a onclick="PowerMonkey.showScheduler()" href="#filter" data-option-value=".design">Schedule Pictures</a></li>
                        <li><a href="#filter" data-option-value=".photography" onclick="PowerMonkey.showManagePictures()">Manage Scheduled Pictures</a></li>
                        <li><a href="#filter" onclick="PowerMonkey.showLinks();">Links</a></li>
                        <li><a href="#filter" onclick="PowerMonkey.showStatus();">Status</a></li>
                    </ul>
                </nav>
                <!-- End Filter -->
            </div>
            
            <div class="col-sm-9">
            	<div class="row">
                	<section id="projects">
                        <div >
                            <h4 style="text-align: center;">Loading Pages...</h4>
                        </div>
                    	<div class="progress" style="color: white">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                <span class="sr-only">0% Complete</span>
                              </div>
                        </div>
                    </section>
                    
            	</div>
            </div>
        </div>
        <!-- End Portfolio Projects -->
    </div>
</div>
<!-- End Our Work Section -->

<!-- Footer -->
<footer>
	<p class="credits">&copy;2014 Pow3r Monk3y. <a href="https://hillandclark.com/pow3rmonk3y/" >Pow3r Monk3y</a> by <a href="https://www.hillandclark.com/" >Hill &amp; Clark Software Development</a></p>
</footer>
<!-- End Footer -->

<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->


<!-- Js -->
<script src="_include/js/bootstrap.min.js"></script> <!-- Bootstrap -->
<script src="_include/js/supersized.3.2.7.min.js"></script> <!-- Slider -->
<script src="_include/js/waypoints.js"></script> <!-- WayPoints -->
<script src="_include/js/waypoints-sticky.js"></script> <!-- Waypoints for Header -->
<script src="_include/js/jquery.isotope.js"></script> <!-- Isotope Filter -->
<script src="_include/js/jquery.fancybox.pack.js"></script> <!-- Fancybox -->
<script src="_include/js/jquery.fancybox-media.js"></script> <!-- Fancybox for Media -->
<script src="_include/js/jquery.tweet.js"></script> <!-- Tweet -->
<script src="_include/js/plugins.js"></script> <!-- Contains: jPreloader, jQuery Easing, jQuery ScrollTo, jQuery One Page Navi -->
<script src="_include/js/main.js"></script> <!-- Default JS -->
<script src="_include/js/keypress.js"></script> <!-- KeyPress -->
<!-- End Js -->

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="_include/fileUpload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<!-- blueimp Gallery script -->
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="_include/fileUpload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="_include/fileUpload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="_include/fileUpload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="_include/fileUpload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="_include/fileUpload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="_include/fileUpload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="_include/fileUpload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="_include/fileUpload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="_include/fileUpload/js/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="_include/fileUpload/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->

<script src="_include/js/toggles.min.js"></script>


<!--load powermonkey logged in css -->
<script>
    $('head').append('<link rel="stylesheet" href="_include/css/logged-in.css">');
</script>