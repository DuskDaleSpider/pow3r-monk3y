<?php

require_once( '../php/PowerMonkey/User.php' );
require_once( '../php/PowerMonkey/ConnectToDB.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$train = $_GET['TrainGroup'];
$isAdmin = false;
$isMember = false;

if($user != null && $user != ""){
	if($train != null && $train != ""){

		$trainGroup = null;
		$trainAdmins = array();
		$trainMembers = array();

		$connection = ConnectToDB::connect();
		$sql = 'SELECT * FROM TrainGroups WHERE ID='.$train.';';

		$sql .= 'SELECT tga.UserID, un.Name '.
				'FROM TrainGroupAdmins AS tga '.
				'JOIN UserNames AS un ON tga.UserID=un.UserID '.
				'WHERE TrainID='.$train.';';

		$sql .= 'SELECT tgm.UserID, un.Name '.
				'FROM TrainGroupMembers AS tgm '.
				'JOIN UserNames AS un ON tgm.UserID=un.UserID '.
				'WHERE TrainID='.$train.';';
			   
		$connection->multi_query($sql);
		//get train row
		$trainResult = $connection->store_result();
		$trainGroup = $trainResult->fetch_assoc();

		$connection->next_result(); //Move to Admins
		$admins = $connection->store_result();
		while($admin = $admins->fetch_assoc()){
			array_push($trainAdmins, $admin);
		}

		$connection->next_result(); //Move to Members
		$members = $connection->store_result();
		while($member = $members->fetch_assoc()){
			array_push($trainMembers, $member);
		}

		$connection->close();

		//check to see if user is an admin of this train group
		foreach($trainAdmins as $admin){
			if($admin['UserID'] == $user->getUserID()){
				$isAdmin = true;
				$isMember = true;
				break;
			}
		}

		//if not an admin, see if user is a member
		if(!$isAdmin){
			foreach($trainMembers as $member){
				if($member['UserID'] == $user->getUserID()){
					$isMember = true;
					break;
				}
			}
		}

		//start html response
		?>

		<div class="train-group">
			<section class="col-sm-12">
				<div class="train-group-name">
					<h2><?echo $trainGroup['Name'];?></h2>
				</div>
			</section>
			
			<section class="col-sm-12 admin-controls">
				<div class="accordion" id="accordionArea">
						<div class="accordion-group">
						<? if($isAdmin){ ?>
							<div class="accordion-heading accordionize">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#group-buttons">
									Admin Controls <span class="font-icon-arrow-simple-down"></span>
								</a>
							</div>
							<div id="group-buttons" class="accordion-body collapse">
								<div class="accordion-inner">
									<a class="button button-small button-blue" onclick="PowerMonkey.startTrain()" href="javascript:void(0);">Start Train</a>
									<a class="button button-small button-blue" onclick="PowerMonkey.stopTrain()" href="javascript:void(0);">Stop Train</a>
									<a class="button button-small button-red" onclick='PowerMonkey.confirmBox(PowerMonkey.deleteTrainGroup)' href="javascript:void(0);">Delete Train Group</a>
								</div>
							</div>
						<?}else if($isMember){?>
							<div class="accordion-heading accordionize">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#group-buttons">
									Controls <span class="font-icon-arrow-simple-down"></span>
								</a>
							</div>
							<div id="group-buttons" class="accordion-body collapse">
								<div class="accordion-inner">
									<a class="button button-small button-blue" onclick="PowerMonkey.stopTrain()" href="javascript:void(0);">Stop Train</a>
								</div>
							</div>
						<?}//end if?>
						</div>
					</div>
			</section>
			<section class="col-sm-6">
				<div class="comments">
					<ul>
						<!--
							<li>
								<span class="username <?echo $commenterIsAdmin ? 'admin' : 'member';?>"><?echo $comment['Name'];?></span>: <?echo $comment['Comment'];?>
							</li>
						-->	
					</ul>
				</div>
				<div class="comment-input">
					<input type="text" id="commentTextBox" placeholder="Write a comment.." onkeydown="PowerMonkey.onEnter(event, PowerMonkey.submitComment)" />
					<a href="javascript:void(0);" onclick="PowerMonkey.submitComment()" class="button button-small button-blue">Send</a>
				</div>
			</section>
			<section class="col-sm-6 people">
				<div>
					<div class="admins">
						<h4>Admins:</h4>
						<ul>
						<?
							foreach($trainAdmins as $admin){
							?>
								<li><?echo $admin['Name'];?></li>
							<?
							}
						?>
						</ul>
					</div>
					<div class="members">
						<h4>Members:</h4>
						<ul>
						<?
							foreach($trainMembers as $member){
							?>
								<li><?echo $member['Name'];?></li>
							<?
							}
						?>
						</ul>
					</div>
				</div>
			</section>
			<section class="col-sm-12 added-pages">
			<?
				if(!$isAdmin && !$isMember){
				?>
					<div class="add-pages-button">
						<a href="javascript:void(0)" onclick="PowerMonkey.joinTrainGroup()" class="button button-small button-blue">
							Join Group
						</a> 
					</div>
				<?
				}else if($trainGroup['TrainType'] == 'Page'){
					$connection = ConnectToDB::connect();
					$sql = 'SELECT ptl.PageID, ptl.PageName, un.Name, ptl.UserID '.
						   'FROM PageTrainLinks as ptl '.
						   'JOIN UserNames as un ON ptl.UserID=un.UserID '.
						   'WHERE TrainID='.$trainGroup['ID'];
					$trainLinks = $connection->query($sql);
				?>
					<div class="add-pages-button">
						<a href="javascript:void(0);" onclick="PowerMonkey.showAddPagesToTrainGroup()" class="button button-small button-blue">
							Add your pages
						</a>	
					</div>
					<table>
						<thead>
							<tr>
								<td colspan="2">Pages:</td>
							</tr>
							<tr>
								<td>Page</td>
								<td>Added By</td>
								<td>Remove</td>
							</tr>
						</thead>
						<tbody>
						<?
							if($trainLinks->num_rows > 0){
								while($trainLink = $trainLinks->fetch_assoc()){
									$pageurl = 'https://www.facebook.com/'.$trainLink['PageID'];
								?>
									<tr>
										<td><a href="<?echo $pageurl;?>" target="_blank"><?echo $trainLink['PageName'];?></a></td>
										<td><?echo $trainLink['Name'];?></td>
										<td>
											<?if($isAdmin || ($trainLink['UserID'] == $user->getUserID())){?>
												<a href="javascript:void(0)" class="button button-red button-small" onclick="PowerMonkey.removePageFromTrainGroup(<?echo $trainLink['PageID'];?>)">Remove</a>
											<?}else{?>
												<a href="javascript:void(0)" class="button button-red button-small disabled">Remove</a>
											<?}?>
										</td>
									</tr>
								<?
								}//end while
							}else{
							?>
								<tr>
									<td colspan="2">
										<h4>No Pages have been added! :c</h4>
									</td>
								</tr>
							<?
							}
						?>
						</tbody>
					</table>		
				<?
				}
				?>
				</section>
				<?if(!$isAdmin && $isMember){?>
				<section class="col-sm-12">
					<a class="button button-small button-red" href="javascript:void(0);" onclick="PowerMonkey.confirmBox(PowerMonkey.leaveTrainGroup)">
						Leave Group
					</a>
				</section>
				<?}//end if?>
		</div>
		<?
	}else{
		$response = array(
			"error" => "Invalid parameters"
		);
		die(json_encode($response));
	}
}else{
	$response = array(
		"error" => "User is not logged in"
	);
	die(json_encode($response));
}
?>