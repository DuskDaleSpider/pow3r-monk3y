<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folderType = $_GET['folderType'];
$FOLDER_TYPES = array(
	"Photos" => 0,
	"Links" => 1,
	"Status" => 2,
	"Videos" => 3
);

if($folderType == null OR $folderType == "" OR !is_numeric($folderType)){
	$folderType = $FOLDER_TYPES['Photos'];
}

if($user != null && $user != ""){

	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM PageFolders WHERE UserID=\''.$user->getUserID().'\' AND FolderType='.$folderType;
	$folders = $connection->query($sql);
	$connection->close();

	?>

		<div class="folders-wrapper">
			<div class="folder-controls">
				<a class="button button-small button-blue" onclick="PowerMonkey.createFolder(<? echo $folderType; ?>)" href="#projects">Create Grouping</a>
			</div>
			<div style="text-align: center;">
				<h2><? switch($folderType){
						case 0:
							echo 'Photo';
							break;
						case 1:
							echo 'Link';
							break;
						case 2:
							echo 'Status';
							break;
					}?> Groupings</h2>
			</div>
			<div class="folders">
			<?
			if($folders->num_rows > 0){
				while($folder = $folders->fetch_assoc()){
					$id = $folder['ID'];
					$name = $folder['Name'];
					?>
					<a class="button button-small button-blue" onclick='PowerMonkey.showFolder("<? echo $id; ?>")' href="#projects"><? echo $name ?></a>
					<?
				}
			}else{
				?>
				<div style="text-align: center; font-size: 2em;" id="no-folders">You do not have any groupings.</div>
				<?
			}
			?>
			</div>
		</div>
	<?
}else{
	$response = array(
		"error" => "user is not logged in"
	);
	die(json_encode($response));
}
?>