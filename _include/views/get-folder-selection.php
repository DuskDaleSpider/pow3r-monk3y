<?php

require_once('../php/PowerMonkey/User.php');

use PowerMonkey\User;

session_start();

$user = $_SESSION['User'];

if($user != null && $user != ""){
	?>
		<div class="folders-wrapper">
			<div class="folders">
					<a class="button button-small button-blue" onclick='PowerMonkey.showFolderGroup(0)' href="#projects">Photos</a>
					<a class="button button-small button-blue" onclick='PowerMonkey.showFolderGroup(1)' href="#projects">Links</a>
					<a class="button button-small button-blue" onclick='PowerMonkey.showFolderGroup(2)' href="#projects">Status</a>
					<!--<a class="button button-small button-blue" onclick='PowerMonkey.showFolderGroup(3)' href="#projects">Videos</a>-->
			</div>
		</div>
	<?
}else{
	$response = array(
		"error" => "user is not logged in"
	);
	die(json_encode($response));
}
?>