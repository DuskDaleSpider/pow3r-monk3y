<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_GET['Folder'];

if($user != null && $user != "" && $folder != null && $folder != ""){
	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM ScheduledStatus GROUP BY Message HAVING FolderID='.$folder.' ORDER BY TimeToPost ASC';
	$status = $connection->query($sql);
	$connection->close();
?>
	<div class="page-activity">
		<table>
			<?if($status->num_rows > 0){?>
				<thead>
					<tr>
						<td>Status</td>
						<td>Posts in</td>
					</tr>
				</thead>
			<?}//end if?>
			<tbody>
			<?
				if($status->num_rows > 0){
					$now = time();
					while($row = $status->fetch_assoc()){
						$timeToPost = ($row['TimeToPost'] - $now) / 60;
						$message = $row['Message'];
						?>
						<tr>
							<td><?echo $message;?></td>
							<td class="min-to-post"><?echo number_format($timeToPost, 2, '.', ',');?> Minutes</td>
						</tr>
						<?
					}
				}else{
				?>
					<tr>
						<td colspan="0"><h4 class="no-posts">No Scheduled Status</h4></td>
					</tr>
				<?
				}
			?>
			</tbody>
		</table>
	</div>
<?}?>