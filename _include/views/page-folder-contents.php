<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_GET['Folder'];

if($user != null && $user != ""){
	if($folder != null && $folder != ""){
		$connection = ConnectToDB::connect();
		$sql = 'SELECT * FROM PageFolderContents WHERE FolderID='.$folder;
		$contents = $connection->query($sql);
		$sql = 'SELECT * FROM PageFolders WHERE ID='.$folder;
		$folderResult = $connection->query($sql);
		$folderResult = $folderResult->fetch_assoc();
		$connection->close();
		?>
			<div class="folder-contents-wrapper">
				<div class="folder-contents-controls">
					<div class="accordion" id="accordionArea">
						<div class="accordion-group">
							<div class="accordion-heading accordionize">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#folder-buttons">
									Controls <span class="font-icon-arrow-simple-down"></span>
								</a>
							</div>
							<div id="folder-buttons" class="accordion-body collapse">
								<div class="accordion-inner">
									<a class="button button-small button-blue" onclick="PowerMonkey.showFolder(<?php echo $folder; ?>)" href="javascript:void(0);">Pages</a>
									<?
										if($folderResult['FolderType'] == 0){	
									?>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderUploader()' href="javascript:void(0);">Upload Photos</a>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderScheduler()' href="javascript:void(0);">Schedule Photos</a>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderScheduledPhotos()' href="javascript:void(0);">Manage Scheduled Photos</a>
									<?
										}else if($folderResult['FolderType'] == 1){
									?>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderLinks()' href="javascript:void(0);">Links</a>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderLinkScheduler()' href="javascript:void(0);">Schedule Links</a>
										<a class="button button-small button-blue" onclick='PowerMonkey.showScheduledFolderLinks()' href="javascript:void(0);">Scheduled Links</a>
									<?
										}else if($folderResult['FolderType'] == 2){
									?>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderStatus()' href="javascript:void(0);">Status</a>
										<a class="button button-small button-blue" onclick='PowerMonkey.showFolderStatusScheduler()' href="javascript:void(0);">Schedule Status</a>
										<a class="button button-small button-blue" onclick='PowerMonkey.showScheduledFolderStatus()' href="javascript:void(0);">Scheduled Status</a>									
									<?
										}
									?>
									<a class="button button-small button-red" onclick='PowerMonkey.confirmBox(PowerMonkey.deleteFolder)' href="javascript:void(0);">Delete Grouping</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="folder-contents">
					<div class="folder-title">
						<h2><? echo $folderResult['Name'];?></h2>
					</div>
					<table class="folder-pages">
						<tbody>
							<?
								if($contents->num_rows > 0){
									while($page = $contents->fetch_assoc()){
										$pageID = $page['PageID'];
										?>
											<tr class="folder-page">
												<td><h4><? echo $page['PageName']; ?></h4></td>
												<td><a class="button button-small button-red" onclick='PowerMonkey.confirmBox(PowerMonkey.removeFolderPage, {page: "<? echo $pageID; ?>"})' href="javascript:void(0);">Remove</a></td>
											</tr>
										<?
									}
								}else{
									?>
										<tr>
											<td><h4 id="no-folder-page" style="text-align: center;">There are no pages in this grouping.</h4></td>
										</tr>
									<?
								} 
							?>
						</tbody>
						<thead>
							<tr>
								<td colspan="0">
									<a class="button button-small button-blue" onclick='PowerMonkey.addPagesToFolder()' href="javascript:void(0);" style="width: 100%">Add Pages</a>
								</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		<?
	}else{
		$response = array(
			"error" => "invalid parameters"
		);
		die(json_encode($response));
	}
}else{
	$response = array(
		"error" => "user is not logged in"
	);
	die(json_encode($response));
}

?>