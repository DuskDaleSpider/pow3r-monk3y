<?php

require_once( '../php/PowerMonkey/ConnectToDB.php' );
require_once( '../php/PowerMonkey/User.php' );

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$page = $_GET['Page'];

if($user != null && $user != ""){
	if($page == null || $page == "") { $page = 0; }
	//get active trains
	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM TrainGroups LIMIT '. $page * 10 .',10';
	$trainGroups = $connection->query($sql);
	$connection->close();

?>

	<div class="trains-wrapper">
		<div class="train-controls">
			<a class="button button-small button-blue" onclick="PowerMonkey.showCreateTrainGroup()">Create Train Group</a>
		</div>
		<?php
		if($trainGroups->num_rows > 0){
		?>
			<table class="trains">
				
				<tbody>
					<?php while($row = $trainGroups->fetch_assoc()){ $trainID = $row['ID'];?>
						<tr onclick='PowerMonkey.showTrainGroupDetails("<? echo $trainID; ?>")'>
							<td>
								<span class="train-name">
									<? echo $row['Name']; ?>
								</span>
							</td>
							<td>
								<? echo $row['Delay']; ?> min.
							</td>
							<td>
								<? echo $row['Gap']; ?> min.
							</td>
							<td>
								<? echo $row['WillDelete'] > 0 ? $row['DeleteDelay'] . ' min' : 'N/A'; ?>
							</td>
						</tr>
					<? } ?>
				</tbody>
				<thead>
					<tr>
						<td colspan="5">
							Trains
						</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>Delay</td>
						<td>Gap</td>
						<td>Delete</td>
					</tr>
				</thead>
			</table>
		<?php
			}else{
				?>
					<div class="trains-error">
						<span>
							No Open Train Groups. :C
						</span>
					</div>
				<?
			} //end if
		?>
	</div>

<?php
}else{
	$response = array(
		"error" => "User is not logged in"
	);
	die(json_encode($response));
}
?>