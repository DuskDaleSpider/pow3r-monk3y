<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$page = $_GET['Page'];

if($user != null && $user != "" && $page != null && $page != ""){
	$now = time();
	$connection = ConnectToDB::connect();
	$sql = 'SELECT * FROM ScheduledPhotos WHERE UserID="'.$user->getUserID().'" AND PageID="'.$page.'" AND TimeToPost > '.$now.' ORDER BY TimeToPost';
	$scheduledPhotos = $connection->query($sql);
	$sql = 'SELECT * FROM ScheduledLinks WHERE UserID="'.$user->getUserID().'" AND PageID="'.$page.'" AND TimeToPost > '.$now.' ORDER BY TimeToPost';
	$scheduledLinks = $connection->query($sql);
	$sql = 'SELECT * FROM ScheduledStatus WHERE UserID="'.$user->getUserID().'" AND PageID="'.$page.'" AND TimeToPost > '.$now.' ORDER BY TimeToPost';
	$scheduledStatus = $connection->query($sql);
	$now = time();
	?>

		<div class="page-activity">
			<div class="accordion" id="activity-accordion">
				<div class="accordion-group">
					<div class="accordion-heading accordionize">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#activity-accordion"
							href="#photo-body">
							Scheduled Photos <span class="font-icon-arrow-simple-down"></span>
						</a>
					</div>
					<div class="accordion-body collapse" id="photo-body">
						<div class="accordion-inner">
							<table class="activity-table">
								<tbody>
								<?
									if($scheduledPhotos && $scheduledPhotos->num_rows > 0){
										while($photo = $scheduledPhotos->fetch_assoc()){
											$picLink = $photo['PicLink'];
											$minToPost = (($photo['TimeToPost'] - $now) / 60);
											?>
											<tr>
												<td>
													<img src="<?php echo $picLink; ?>" />	
													<span class="min-to-post"><?echo number_format($minToPost, 2, '.', ',').' Minutes';?></span>
												</td>
											</tr>
											<?
										}	
									}else{
										?>
											<tr>
												<td><h4 class="no-posts"> No Scheduled Photos</h1></td>
											</tr>
										<?
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="accordion-heading accordionize">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#activity-accordion"
							href="#link-body">Scheduled Links <span class="font-icon-arrow-simple-down"></span></a>
					</div>
					<div class="accordion-body collapse" id="link-body">
						<div class="accordion-inner">
							<table class="activity-table">
								<?if($scheduledLinks->num_rows > 0){?>
								<thead>
									<tr>
										<td>Link</td>
										<td>Posts in</td>
									</tr>
								</thead>
								<?}//end if?>
								<tbody>
								<?
									if($scheduledLinks->num_rows > 0){
										while($row = $scheduledLinks->fetch_assoc()){
											$minToPost = ($row['TimeToPost'] - $now) / 60;
											$link = $row['Link'];
										?>
											<tr>
												<td>
													<a target="_blank" href="<?php echo $link; ?>">
													<?echo strlen($link) > 51 ? substr($link, 0, 51) . '...' : $link;?></td>
													</a>
												<td class="min-to-post"><?echo number_format($minToPost, 2, '.', ',');?> Minutes</td>
											</tr>
										<?
										}
									}else{
									?>
										<tr>
											<td colspan="0">
												<h4 class="no-posts">No Scheduled Links</h4>
											</td>
										</tr>
									<?
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="accordion-heading accordionize">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#activity-accordion"
							href="#status-body">Scheduled Status <span class="font-icon-arrow-simple-down"></span></a>
					</div>
					<div class="accordion-body collapse" id="status-body">
						<div class="accordion-inner">
							<table class="activity-table">
								<?if($scheduledStatus->num_rows > 0){?>
								<thead>
									<tr>
										<td>Status</td>
										<td>Posts in</td>
									</tr>
								</thead>
								<?}//end if?>
								<tbody>
								<?
									if($scheduledStatus->num_rows > 0){
										while($status = $scheduledStatus->fetch_assoc()){
											$minToPost = ($status['TimeToPost'] - $now) / 60;
										?>
											<tr>
												<td><?echo $status['Message']?></td>
												<td class="min-to-post"><?echo number_format($minToPost, 2, '.', ',');?> Minutes</td>
											</tr>
										<?
										}
									}else{
									?>
										<tr>
											<td colspan="0">
												<h4 class="no-posts">No Scheduled Status</h4>
											</td>
										</tr>
									<?
									}
								?>
								</tbody>
							</table>
						</div>
					</div> 
				</div>
			</div>
		</div>

	<?
}

?>