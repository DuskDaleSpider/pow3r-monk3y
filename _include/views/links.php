<?php
require_once ('../php/PowerMonkey/User.php');

use PowerMonkey\User;

session_start();

$user = $_SESSION["User"];
$page = $_GET["Page"];
$links = array();

if($user != null && $user != ""){
	//read links for user's page
	$pathStructure = '../fileUpload/server/php/files/'.$user->getUserID().'/links';
	if(!file_exists($pathStructure)) mkdir($pathStructure, 0775, true);
	$filePath = '../fileUpload/server/php/files/'.$user->getUserID().'/links/'.$page.'.dat';
	$linksFile = null;
	if(!file_exists($filePath)){
		$linksFile = fopen($filePath, "w");
	}else{
		$linksFile = fopen($filePath, "r");
		if(filesize($filePath) > 0){
			$links = explode(",", fread($linksFile, filesize($filePath)));
		}
	}
	fclose($linksFile);
}else{
	$response = array(
		"error" => "User is not logged in!"
	);
	die(json_encode($response));
}
?>
<div id="links-wrapper">
	<section class="links-input">
		<div class="link-description">
			Type in the links you want to add that are seperated by commas.
		</div>
		<textarea class="links-textbox" />
		<a href="#links-wrapper" class="button button-small button-blue"
		   onclick="PowerMonkey.submitLinks()">Add Links</a>
		<a href="#links-wrapper" class="button button-small button-blue"
		   onclick="PowerMonkey.showLinkScheduler()">Schedule Links</a>
	</section>
	<section class="links-table-wrapper">
		<table>
			<thead>
				<tr>
					<td>Your Links:</td>
					<td>
						<a href="#links-wrapper" class="button button-small button-red" onclick="PowerMonkey.confirmBox(PowerMonkey.deleteAllLinks)">Delete All</a>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($links as $link){
						$shortlink = null;
						try{
							$pos = strpos($link, '/', 10);
							if($pos > 0){
								$shortlink = substr($link, 0, $pos);
							}
						}catch(Exception $e){

						}
						?>
						<tr>
							<td>
								<a href="<?php echo $link; ?>" target="_blank">
									<?php if($shortlink != "" && $shortlink != null) echo $shortlink; else echo $link; ?>
								</a>
							</td>
							<td>
								<a href="#links-wrapper" class="button button-small button-red"
								   onclick='PowerMonkey.confirmBox(PowerMonkey.deleteLink, {link: "<?php echo $link; ?>", element: this});'>
								   	Delete
								</a>
							</td>
						</tr>
						<?
					}
				?>
			</tbody>
		</table>
	</section>
</div>	