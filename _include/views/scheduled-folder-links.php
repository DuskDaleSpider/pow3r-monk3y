<?php

require_once('../php/PowerMonkey/User.php');
require_once('../php/PowerMonkey/ConnectToDB.php');

use PowerMonkey\User;
use PowerMonkey\ConnectToDB;

session_start();

$user = $_SESSION['User'];
$folder = $_GET['Folder'];

if($user != null && $user != "" && $folder != null && $folder != ""){
	$connection = ConnectToDB::connect();
	$now = time();
	$sql = 'SELECT * FROM ScheduledLinks GROUP BY Link HAVING FolderID='.$folder.' AND TimeToPost>'.$now.' ORDER BY TimeToPost ASC';
	$links = $connection->query($sql);
	$connection->close();
?>
	<div class="page-activity">
		<table>
			<?if($links->num_rows > 0){?>
				<thead>
					<tr>
						<td>Link</td>
						<td>Posts in</td>
					</tr>
				</thead>
			<?}//end if?>
			<tbody>
			<?
				if($links->num_rows > 0){
					$now = time();
					while($row = $links->fetch_assoc()){
						$timeToPost = ($row['TimeToPost'] - $now) / 60;
						$link = $row['Link'];
						?>
						<tr>
							<td><a target="_blank" href="<?echo $link;?>"><?echo strlen($link) > 51 ? substr($link, 0, 51).'...' : $link;?></td>
							<td class="min-to-post"><?echo number_format($timeToPost, 2, '.', ',');?> Minutes</td>
						</tr>
						<?
					}
				}else{
				?>
					<tr>
						<td colspan="0"><h4 class="no-posts">No Scheduled Links</h4></td>
					</tr>
				<?
				}
			?>
			</tbody>
		</table>
	</div>
<?}?>