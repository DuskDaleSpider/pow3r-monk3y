<?php
	session_start();
?>
<!-- This section is for Splash Screen -->
<div class="ole">
<section id="jSplash">
	<div id="circle"></div>
</section>
</div>
<!-- End of Splash Screen -->

<!-- About Section -->
<div id="about" class="page-alternate">
<div class="container">
        <!-- Start Profile -->
    	<div class="profile">
        	<div class="image-wrap" onclick="PowerMonkey.subscribe();">
                <div class="hover-wrap">
                    <span class="overlay-img"></span>
                    <span class="overlay-text-thumb">Subscribe</span>
                </div>
                <img src="_include/img/profile/profile-03.jpg" alt="Subscribe" />
           </div>
        </div>
        <!-- End Profile -->
</div>
<footer style="position: absolute; bottom: 0; left: 0;">
	<p class="credits">&copy;2013 Pow3r Monk3y. <a href="" title="">Pow3r Monk3y</a> by <a href="" title="Hill &amp; Clark">Hill &amp; Clark</a></p>
</footer>
</div>
<!-- End About Section -->

<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->

<!-- Js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> <!-- jQuery Core -->
<script src="_include/js/bootstrap.min.js"></script> <!-- Bootstrap -->
<script src="_include/js/supersized.3.2.7.min.js"></script> <!-- Slider -->
<script src="_include/js/waypoints.js"></script> <!-- WayPoints -->
<script src="_include/js/waypoints-sticky.js"></script> <!-- Waypoints for Header -->
<script src="_include/js/jquery.isotope.js"></script> <!-- Isotope Filter -->
<script src="_include/js/jquery.fancybox.pack.js"></script> <!-- Fancybox -->
<script src="_include/js/jquery.fancybox-media.js"></script> <!-- Fancybox for Media -->
<script src="_include/js/jquery.tweet.js"></script> <!-- Tweet -->
<script src="_include/js/plugins.js"></script> <!-- Contains: jPreloader, jQuery Easing, jQuery ScrollTo, jQuery One Page Navi -->
<script src="_include/js/main.js"></script> <!-- Default JS -->

<script>
	$('head').append('<link rel="stylesheet" href="_include/css/logged-out.css">');
</script>
<!-- End Js -->