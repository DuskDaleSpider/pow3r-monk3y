<?php

require_once ( '_include/php/Facebook/Entities/AccessToken.php' );
require_once ( '_include/php/Facebook/FacebookSession.php' );
require_once ( '_include/php/Facebook/FacebookSDKException.php');
require_once ( '_include/php/Facebook/FacebookRequestException.php');
require_once ( '_include/php/Facebook/FacebookOtherException.php');
require_once ( '_include/php/Facebook/FacebookAuthorizationException.php');
require_once ( '_include/php/Facebook/HttpClients/FacebookHttpable.php' );
require_once ( '_include/php/Facebook/HttpClients/FacebookCurl.php' );
require_once ( '_include/php/Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once ( '_include/php/Facebook/FacebookRequest.php' );
require_once ( '_include/php/Facebook/FacebookResponse.php' );
require_once ( '_include/php/Facebook/GraphObject.php' );
require_once ( '_include/php/Facebook/GraphSessionInfo.php' );
require_once ( '_include/php/Facebook/FacebookServerException.php');
require_once ( '_include/php/Facebook/FacebookThrottleException.php');
require_once ( '_include/php/Facebook/FacebookRedirectLoginHelper.php');
require_once ( '_include/php/PowerMonkey/User.php' );

use Facebook\Entities\FacebookAccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;
use Facebook\FacebookOtherException;
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\FacebookRedirectLoginHelper;
use PowerMonkey\User;

session_start();

FacebookSession::setDefaultApplication('1397437400572800', '55aafefa842b471c60966437f38ccde3');

//Facebook Login Helper that points to V2 directory of The Dream Machine
$helper = new FacebookRedirectLoginHelper("https://www.hillandclark.com/pow3rmonk3y/");

$session = null;

//see if a FB Session already exists
if($_GET["Token"] != null && $_GET["Token"] != ""){ 
  try{
    $accessToken = $_GET["Token"];
    if($accessToken != null && $accessToken != ""){
      $_SESSION["fb_token"] = $accessToken;
      $session = new FacebookSession($accessToken);
      $user = (new FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject()->asArray();
      $page = 'me';
      $userId = $user["id"];
      $isSubscribed = false;
      $_SESSION["User"] = new User($userId, $accessToken, $isSubscribed, $session);
    }
  }catch(Exception $e){
    $session = null;
  }
}else if(isset($_SESSION) && isset($_SESSION['fb_token'])) {
  $session = new FacebookSession($_SESSION['fb_token']);
}else {
	// no session exists
	try {
		//try to get session from code in url
		$session = $helper->getSessionFromRedirect();

		if($session != null){
			// save the session
			$_SESSION['fb_token'] = $session->getToken();
			// create a session using saved token or the new one we generated at login
			$session = new FacebookSession($session->getToken());
		}
	} catch( FacebookRequestException $ex ) {
		//facebook returns an error
	} catch( Exception $ex ) {
		//Some other error
	}//End try/catch

}//End if

$_SESSION["loginURL"] = $helper->getLoginUrl(array("manage_pages", "publish_pages"));

if($session){
	try{
    $user = (new FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject()->asArray();
  	$page = 'me';
    $userId = $user["id"];
    $accessToken = $session->getToken();
    $isSubscribed = false;
    $_SESSION["User"] = new User($userId, $accessToken, $isSubscribed, $session);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://www.hillandclark.com/pow3rmonk3y/_include/php/CheckUser.php?User=".$user["id"]."&AccessToken=".$session->getToken());
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec($ch);
    $response = json_decode($response);
    $_SESSION['User'] = new User($_SESSION['User']->getUserID(), $response->token, $response->isSubscribed, new FacebookSession($response->token));
    curl_close($ch);

  }catch(Facebook\FacebookAuthorizationException $e){
    $session = false;
  }
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Pow3r Monk3y!</title>   

<!--Favicon-->
<link rel="icon" type="image/ico" href="_include/img/pow3rmonk3y.ico">

<!-- So Meta -->
<meta property="fb:app_id" content="1397437400572800" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://www.hillandclark.com/pow3rmonk3y/_include/img/logo.jpg" />
<meta property="og:url" content="https://www.hillandclark.com/pow3rmonk3y/" />
<meta property="og:title" content="Pow3r Monk3y" />
<meta property="og:description" content="The best app to schedule content to your facebook pages!" /> 
<!-- /Meta -->

<!-- Mdobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="320"/>   

<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->


<script>
	var userId =  "<?php echo $user["id"]; ?>";
	var pageId =  "<?php echo $page; ?>";
</script>

<!-- Bootstrap -->
<link href="_include/css/bootstrap.min.css" rel="stylesheet">
<link href="_include/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Main Style -->
<link href="_include/css/main.css" rel="stylesheet">

<!-- Supersized -->
<link href="_include/css/supersized.css" rel="stylesheet">
<link href="_include/css/supersized.shutter.css" rel="stylesheet">

<!-- FancyBox -->
<link href="_include/css/fancybox/jquery.fancybox.css" rel="stylesheet">

<!-- Font Icons -->
<link href="_include/css/fonts.css" rel="stylesheet">

<!-- Shortcodes -->
<link href="_include/css/shortcodes.css" rel="stylesheet">

<!-- Responsive -->
<link href="_include/css/responsive.css" rel="stylesheet">

<!-- Supersized -->
<link href="_include/css/supersized.css" rel="stylesheet">
<link href="_include/css/supersized.shutter.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

<!-- Fav Icon -->
<link rel="shortcut icon" href="#">

<link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="114x114" href="#">
<link rel="apple-touch-icon" sizes="72x72" href="#">
<link rel="apple-touch-icon" sizes="144x144" href="#">

<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="_include/fileUpload/css/jquery.fileupload.css">
<link rel="stylesheet" href="_include/fileUpload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="_include/fileUpload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="_include/fileUpload/css/jquery.fileupload-ui-noscript.css"></noscript>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Yellowtail" />

<link rel="stylesheet" type="text/css" href="_include/css/toggles.css" />
<link rel="stylesheet" type="text/css" href="_include/css/toggles-modern.css" />

<!-- Modernizr -->
<script src="_include/js/modernizr.js"></script>

<!-- jQuery Core -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Stripe JS -->
<script src="https://checkout.stripe.com/checkout.js"></script>

<script src="_include/js/powermonkey.js"></script>

</head>


<body>
<script>
   $(document).ready(function(){
  	window.fbAsyncInit = function() {
    FB.init({
      appId      : '1397437400572800',
      xfbml      : false,
      version    : 'v2.3'
    });
    
    FB.getLoginStatus(function(data){
      if(data.status == "connected"){
        var accessToken = FB.getAuthResponse().accessToken;
        PowerMonkey.init();
      }
    });

  };
  
  
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  });
  
</script>
<?php
	if($session){
    if($response->isSubscribed == "1"){
      include('_include/views/logged-in-body.php');
    }else{
      include('_include/views/subscribe.php');
    }
	}else{
		include('_include/views/logged-out-body.php');
	}
	
?>
</body>
</html>